﻿using System;
using System.Collections.Generic;
using Client.BLL.DTO;

namespace Client.BLL.Interfaces
{
    public interface ISubscriptionService : IDisposable
    {
        void Subscribe(SubscriptionDto subscriptionDto);

        void Unsubscribe(SubscriptionDto subscriptionDto);
        SubscriptionDto GetByUserName(string userName);
        IEnumerable<SubscriptionDto> GetByUserId(string userId);
        IEnumerable<SubscriptionDto> GetAll();
        void UpdateServiceClass(SubscriptionDto subscriptionDto);
    }
}