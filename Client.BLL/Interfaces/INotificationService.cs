﻿using System;
using System.Collections.Generic;
using Client.BLL.DTO;

namespace Client.BLL.Interfaces
{
    public interface INotificationService : IDisposable
    {
        void Add(NotificationDto notificationDto);
        void Dismiss(int? id);
        void DismissAll();
        NotificationDto Get(int? id);
        IEnumerable<NotificationDto> GetAll();
    }
}