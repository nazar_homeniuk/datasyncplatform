﻿using System;
using System.Collections.Generic;
using Client.BLL.DTO;

namespace Client.BLL.Interfaces
{
    public interface ITicketService : IDisposable
    {
        void Create(TicketDto ticket);
        void Delete(int? id);
        IEnumerable<TicketDto> GetAll();
        TicketDto Get(int id);
       IEnumerable<TicketDto> GetAllByClass(string serviceClass);
    }
}