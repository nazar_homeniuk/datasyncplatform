﻿namespace Client.BLL.Interfaces.Identity
{
    public interface IServiceCreator
    {
        IUserService CreateUserService(string connection);
    }
}