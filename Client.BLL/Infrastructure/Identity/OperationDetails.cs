﻿namespace Client.BLL.Infrastructure.Identity
{
    public class OperationDetails
    {
        public OperationDetails(bool succeeded, string message, string property)
        {
            Succeeded = succeeded;
            Message = message;
            Property = property;
        }

        public bool Succeeded { get; }
        public string Message { get; }
        public string Property { get; }
    }
}