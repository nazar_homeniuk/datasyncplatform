﻿using System;

namespace Client.BLL.DTO
{
    public class NotificationDto
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public bool IsDismissed { get; set; }
    }
}