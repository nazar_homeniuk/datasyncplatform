﻿using System;

namespace Client.BLL.DTO
{
    public class TicketDto
    {
        public int Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ArrivalDate { get; set; }
        public string ServiceClass { get; set; }
    }
}
