﻿namespace Client.BLL.DTO
{
    public class SubscriptionDto
    {
        public int Id { get; set; }
        public string SubscriberId { get; set; }
        public string SubscriberUserName { get; set; }
        public string PublisherUserName { get; set; }
        public string PublisherToken { get; set; }
        public string PublisherServiceClass { get; set; }
    }
}