﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Client.BLL.DTO.Identity;
using Client.BLL.Infrastructure.Identity;
using Client.BLL.Interfaces.Identity;
using Client.DAL.Entities.Identity;
using Client.DAL.Interfaces.Identity;
using Microsoft.AspNet.Identity;

namespace Client.BLL.Services.Identity
{
    public class UserService : IUserService
    {
        public UserService(IIdentityUnitOfWork uow)
        {
            Database = uow;
        }

        private IIdentityUnitOfWork Database { get; }

        public async Task<OperationDetails> Create(UserDto userDto)
        {
            var user = await Database.UserManager.FindByEmailAsync(userDto.Email);
            if (user == null)
            {
                user = new ApplicationUser
                {
                    Email = userDto.Email,
                    UserName = userDto.Email
                };
                var result = await Database.UserManager.CreateAsync(user, userDto.Password);
                if (result.Errors.Any()) return new OperationDetails(false, result.Errors.FirstOrDefault(), "");

                await Database.UserManager.AddToRoleAsync(user.Id, userDto.Role);
                var clientProfile = new ClientProfile
                {
                    Id = user.Id,
                    Name = userDto.Name
                };
                Database.ClientManager.Create(clientProfile);
                return new OperationDetails(true, "Registration was successful", "");
            }

            return new OperationDetails(false, "User with this email already exists", "Email");
        }

        public async Task<ClaimsIdentity> Authenticate(UserDto userDto)
        {
            ClaimsIdentity claim = null;
            var user = await Database.UserManager.FindAsync(userDto.Email, userDto.Password);
            if (user != null)
                claim = await Database.UserManager.CreateIdentityAsync(user,
                    DefaultAuthenticationTypes.ApplicationCookie);

            return claim;
        }

        public async Task SetInitialData(UserDto adminDto, List<string> roles)
        {
            foreach (var roleName in roles)
            {
                var role = await Database.RoleManager.FindByNameAsync(roleName);
                if (role == null)
                {
                    role = new ApplicationRole
                    {
                        Name = roleName
                    };
                    await Database.RoleManager.CreateAsync(role);
                }
            }

            await Create(adminDto);
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}