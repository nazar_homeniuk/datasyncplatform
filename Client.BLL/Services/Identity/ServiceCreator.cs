﻿using Client.BLL.Interfaces.Identity;
using Client.DAL.Repositories.Identity;

namespace Client.BLL.Services.Identity
{
    public class ServiceCreator : IServiceCreator
    {
        public IUserService CreateUserService(string connection)
        {
            return new UserService(new IdentityUnitOfWork(connection));
        }
    }
}