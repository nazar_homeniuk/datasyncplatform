﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Client.BLL.DTO;
using Client.BLL.Interfaces;
using Client.DAL.Entities;
using Client.DAL.Interfaces;

namespace Client.BLL.Services
{
    public class NotificationService : INotificationService
    {
        public NotificationService(IUnitOfWork uow)
        {
            Database = uow;
        }

        private IUnitOfWork Database { get; }

        public void Add(NotificationDto notificationDto)
        {
            Database.Notifications.Create(new Notification
            {
                Content = notificationDto.Content,
                Date = notificationDto.Date,
                UserName = notificationDto.UserName,
                IsDismissed = false
            });
            Database.Save();
        }

        public void Dismiss(int? id)
        {
            if (id == null) throw new ArgumentNullException(nameof(id));

            var notification = Database.Notifications.Get(id.Value);
            if (notification == null) throw new InvalidOperationException("Notification not found");

            notification.IsDismissed = true;
            Database.Notifications.Update(notification);
            Database.Save();
        }

        public void DismissAll()
        {
            var notifications = Database.Notifications.Find(i => i.IsDismissed == false).ToList();
            if (!notifications.Any()) return;
            foreach (var notification in notifications)
            {
                notification.IsDismissed = true;
                Database.Notifications.Update(notification);
            }

            Database.Save();
        }

        public NotificationDto Get(int? id)
        {
            if (id == null) throw new ArgumentNullException(nameof(id));

            var notification = Database.Notifications.Get(id.Value);
            if (notification == null) throw new InvalidOperationException("Notification not found");

            return new NotificationDto
            {
                Content = notification.Content,
                Date = notification.Date,
                UserName = notification.UserName,
                Id = notification.Id,
                IsDismissed = notification.IsDismissed
            };
        }

        public IEnumerable<NotificationDto> GetAll()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Notification, NotificationDto>()).CreateMapper();
            return mapper.Map<IEnumerable<Notification>, List<NotificationDto>>(Database.Notifications.GetAll());
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}