﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Client.BLL.DTO;
using Client.BLL.Interfaces;
using Client.DAL.Entities;
using Client.DAL.Interfaces;

namespace Client.BLL.Services
{
    public class SubscriptionService : ISubscriptionService
    {
        public SubscriptionService(IUnitOfWork uow)
        {
            Database = uow;
        }

        private IUnitOfWork Database { get; }

        public void Subscribe(SubscriptionDto subscriptionDto)
        {
            if (Database.Subscriptions.Find(i => i.PublisherUserName == subscriptionDto.PublisherUserName)
                .Any()) return;

            Database.Subscriptions.Create(new Subscription
            {
                PublisherToken = subscriptionDto.PublisherToken,
                PublisherUserName = subscriptionDto.PublisherUserName,
                PublisherServiceClass = subscriptionDto.PublisherServiceClass,
                SubscriberId = subscriptionDto.SubscriberId,
                SubscriberUserName = subscriptionDto.SubscriberUserName
            });
            Database.Save();
        }

        public void Unsubscribe(SubscriptionDto subscriptionDto)
        {
            if (Database.Subscriptions.Get(subscriptionDto.Id) == null) return;

            Database.Subscriptions.Delete(subscriptionDto.Id);
            Database.Save();
        }

        public SubscriptionDto GetByUserName(string userName)
        {
            var subscription = Database.Subscriptions.Find(i => i.PublisherUserName == userName).FirstOrDefault();
            if (subscription == null) return null;

            return new SubscriptionDto
            {
                Id = subscription.Id,
                PublisherToken = subscription.PublisherToken,
                PublisherUserName = subscription.PublisherUserName,
                PublisherServiceClass = subscription.PublisherServiceClass,
                SubscriberUserName = subscription.SubscriberUserName,
                SubscriberId = subscription.SubscriberId
            };
        }

        public IEnumerable<SubscriptionDto> GetByUserId(string userId)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Subscription, SubscriptionDto>()).CreateMapper();
            return mapper.Map<IEnumerable<Subscription>, List<SubscriptionDto>>(Database.Subscriptions.GetAll()
                .Select(i => i).Where(i => i.SubscriberId == userId));
        }

        public IEnumerable<SubscriptionDto> GetAll()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Subscription, SubscriptionDto>()).CreateMapper();
            return mapper.Map<IEnumerable<Subscription>, List<SubscriptionDto>>(Database.Subscriptions.GetAll());
        }

        public void UpdateServiceClass(SubscriptionDto subscriptionDto)
        {
            var subscription = Database.Subscriptions.Find(i => i.PublisherUserName == subscriptionDto.PublisherUserName).FirstOrDefault();
            if (subscription == null) return;
            subscription.PublisherServiceClass = subscriptionDto.PublisherServiceClass;
            Database.Subscriptions.Update(subscription);
            Database.Save();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}