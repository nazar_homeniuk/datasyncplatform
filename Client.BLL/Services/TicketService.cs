﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Client.BLL.DTO;
using Client.BLL.Interfaces;
using Client.DAL.Entities;
using Client.DAL.Interfaces;

namespace Client.BLL.Services
{
    public class TicketService : ITicketService
    {
        public TicketService(IUnitOfWork uow)
        {
            Database = uow;
        }

        private IUnitOfWork Database { get; }

        public void Create(TicketDto ticket)
        {
            Database.Tickets.Create(new Ticket
            {
                Id = ticket.Id,
                ArrivalDate = ticket.ArrivalDate,
                DepartureDate = ticket.DepartureDate,
                From = ticket.From,
                ServiceClass = ticket.ServiceClass,
                To = ticket.To
            });
            Database.Save();
        }

        public void Delete(int? id)
        {
            if (!id.HasValue) return;
            if (Database.Tickets.Get(id.Value) == null) return;
            Database.Tickets.Delete(id.Value);
            Database.Save();
        }

        public IEnumerable<TicketDto> GetAll()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Ticket, TicketDto>()).CreateMapper();
            return mapper.Map<IEnumerable<Ticket>, List<TicketDto>>(Database.Tickets.GetAll());
        }

        public TicketDto Get(int id)
        {
            var ticket = Database.Tickets.Get(id);
            if (ticket == null)
            {
                return null;
            }

            return new TicketDto
            {
                Id = ticket.Id,
                ArrivalDate = ticket.ArrivalDate,
                DepartureDate = ticket.DepartureDate,
                From = ticket.From,
                ServiceClass = ticket.ServiceClass,
                To = ticket.To
            };
        }

        public IEnumerable<TicketDto> GetAllByClass(string serviceClass)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Ticket, TicketDto>()).CreateMapper();
            return mapper.Map<IEnumerable<Ticket>, List<TicketDto>>(Database.Tickets.GetAll().Select(i => i).Where(i => i.ServiceClass == serviceClass));
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}
