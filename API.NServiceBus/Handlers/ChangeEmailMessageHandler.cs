﻿using System.Threading.Tasks;
using API.NServiceBus.Utils;
using NServiceBus;
using Shared.Enums;
using Shared.NServiceBus;

namespace API.NServiceBus.Handlers
{
    public class ChangeEmailMessageHandler : IHandleMessages<ChangeEmailMessage>
    {
        public async Task Handle(ChangeEmailMessage message, IMessageHandlerContext context)
        {
            using (var restClient = new RestClient())
            {
                await restClient.Execute(message, NotificationType.ChangeEmail);
            }
        }
    }
}