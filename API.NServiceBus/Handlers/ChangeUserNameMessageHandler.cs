﻿using System.Threading.Tasks;
using API.NServiceBus.Utils;
using NServiceBus;
using Shared.Enums;
using Shared.NServiceBus;

namespace API.NServiceBus.Handlers
{
    public class ChangeUserNameMessageHandler : IHandleMessages<ChangeUserNameMessage>
    {
        public async Task Handle(ChangeUserNameMessage message, IMessageHandlerContext context)
        {
            using (var restClient = new RestClient())
            {
                await restClient.Execute(message, NotificationType.ChangeUserName);
            }
        }
    }
}