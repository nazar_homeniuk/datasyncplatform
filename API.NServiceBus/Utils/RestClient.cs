﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Shared;
using Shared.Enums;
using Shared.NServiceBus;
using Shared.NServiceBus.Interfaces;

namespace API.NServiceBus.Utils
{
    public class RestClient : IDisposable
    {
        private const string ChangeEmailUri = "/api/Account/ChangeEmail";
        private const string ChangeUserNameUri = "/api/Account/ChangeUserName";
        private const string ChangeServiceClassUri = "/api/Account/ChangeServiceClass";
        private const string CustomNotificationUri = "/api/Notify/Post";

        private readonly HttpClient httpClient;

        public RestClient()
        {
            httpClient = new HttpClient {BaseAddress = new Uri(Connections.ApiBaseAddress)};
        }

        public async Task Execute(INotificationMessage notificationMessage, NotificationType notificationType)
        {
            httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + notificationMessage.AccessToken);
            switch (notificationType)
            {
                case NotificationType.ChangeEmail:
                    await httpClient.PostAsJsonAsync(ChangeEmailUri, (ChangeEmailMessage)notificationMessage);
                    break;
                case NotificationType.ChangeServiceClass:
                    await httpClient.PostAsJsonAsync(ChangeServiceClassUri, (ChangeServiceClassMessage)notificationMessage);
                    break;
                case NotificationType.ChangeUserName:
                    await httpClient.PostAsJsonAsync(ChangeUserNameUri, (ChangeUserNameMessage)notificationMessage);
                    break;
                case NotificationType.Custom:
                    await httpClient.PostAsJsonAsync(CustomNotificationUri, (CustomNotificationMessage)notificationMessage);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(notificationType), notificationType, null);
            }
        }

        public void Dispose()
        {
            httpClient.Dispose();
        }
    }
}
