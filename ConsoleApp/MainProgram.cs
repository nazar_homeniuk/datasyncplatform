﻿using ConsoleApp.Pages;
using ConsoleApp.Services;
using ConsoleUI;

namespace ConsoleApp
{
    public class MainProgram : Program
    {
        public MainProgram() : base("ConsoleApp", true)
        {
            NotificationService = new NotificationService("Console", "host=localhost");
            NotificationService.Start().Wait();
            AddPage(new MainMenu(this));
            AddPage(new Login(this));
            AddPage(new Register(this));
            AddPage(new Messaging(this));
            AddPage(new Menu(this));
            AddPage(new Profile(this));
            AddPage(new ChangeEmail(this));
            AddPage(new ChangeUserName(this));
            AddPage(new ChangeServiceClass(this));
            SetPage<MainMenu>();
        }

        public NotificationService NotificationService { get; }
    }
}