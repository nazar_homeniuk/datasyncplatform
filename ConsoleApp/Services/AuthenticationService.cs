﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp.Models;
using Newtonsoft.Json;
using Shared;

namespace ConsoleApp.Services
{
    public class AuthenticationService
    {
        public static User User { get; private set; }

        public static async Task Login(LoginModel loginModel)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Connections.ApiBaseAddress);
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("username", loginModel.Email),
                    new KeyValuePair<string, string>("password", loginModel.Password),
                    new KeyValuePair<string, string>("grant_type", "password")
                });
                var result = await client.PostAsync("token", content);
                if (!result.IsSuccessStatusCode)
                    throw new AuthenticationException($"[{result.StatusCode}] {result.ReasonPhrase}");

                var resultContent = await result.Content.ReadAsStringAsync();
                var model = JsonConvert.DeserializeObject<LoginResponseModel>(resultContent);
                User = new User
                {
                    AccessToken = model.AccessToken,
                    Email = model.UserName,
                    UserName = model.UserName,
                    ServiceClass = model.ServiceClass
                };
            }
        }

        public static async Task Register(RegisterModel registerModel)
        {
            if (registerModel.Password != registerModel.ConfirmPassword)
                throw new AuthenticationException("Passwords do not match!");

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Connections.ApiBaseAddress);
                var content = new StringContent(JsonConvert.SerializeObject(registerModel), Encoding.UTF8,
                    "application/json");
                var result = await client.PostAsync("api/Account/Register", content);
                if (!result.IsSuccessStatusCode)
                    throw new AuthenticationException($"[{result.StatusCode}] {result.ReasonPhrase}");
            }
        }
    }
}