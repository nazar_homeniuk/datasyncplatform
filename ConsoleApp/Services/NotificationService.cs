﻿using System.Threading.Tasks;
using NServiceBus;
using Shared.NServiceBus;

namespace ConsoleApp.Services
{
    public class NotificationService
    {
        private EndpointConfiguration endpointConfiguration;

        private IEndpointInstance endpointInstance;

        public NotificationService(string endpointName, string connectionString)
        {
            Configure(endpointName, connectionString);
        }

        public async Task Start()
        {
            endpointInstance = await Endpoint.Start(endpointConfiguration).ConfigureAwait(false);
        }

        public async Task Stop()
        {
            await endpointInstance.Stop().ConfigureAwait(false);
        }

        public async Task Trigger(IMessage message)
        {
            await endpointInstance.Send(message).ConfigureAwait(false);
        }

        private void Configure(string endpointName, string connectionString)
        {
            endpointConfiguration = new EndpointConfiguration(endpointName);
            var transport = endpointConfiguration.UseTransport<RabbitMQTransport>();
            transport.ConnectionString(connectionString);
            transport.UsePublisherConfirms(true);
            transport.UseDirectRoutingTopology();
            endpointConfiguration.EnableInstallers();
            endpointConfiguration.UsePersistence<InMemoryPersistence>();
            var routing = transport.Routing();
            routing.RouteToEndpoint(typeof(CustomNotificationMessage), "API");
            routing.RouteToEndpoint(typeof(ChangeUserNameMessage), "API");
            routing.RouteToEndpoint(typeof(ChangeEmailMessage), "API");
            routing.RouteToEndpoint(typeof(ChangeServiceClassMessage), "API");
            endpointConfiguration.UseSerialization<NewtonsoftSerializer>();
        }
    }
}