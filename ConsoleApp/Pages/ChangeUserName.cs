﻿using ConsoleApp.Services;
using ConsoleUI;
using ConsoleUI.Core;
using ConsoleUI.IO;
using Shared.NServiceBus;

namespace ConsoleApp.Pages
{
    public class ChangeUserName : Page
    {
        private readonly NotificationService notificationService;

        public ChangeUserName(Program program) : base("Change user name", program)
        {
            notificationService = (Program as MainProgram)?.NotificationService;
        }

        public override void Display()
        {
            base.Display();
            var oldUserName = AuthenticationService.User.UserName;
            var newUserName = Input.ReadString("Enter new user name: ");
            AuthenticationService.User.UserName = newUserName;
            notificationService.Trigger(new ChangeUserNameMessage
            {
                UserName = newUserName,
                AccessToken = AuthenticationService.User.AccessToken,
                NewUserName = newUserName,
                OldUserName = oldUserName
            }).Wait();
            Program.NavigateBack();
        }
    }
}
