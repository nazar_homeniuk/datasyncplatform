﻿using System;
using System.Security.Authentication;
using ConsoleApp.Models;
using ConsoleApp.Services;
using ConsoleUI;
using ConsoleUI.Core;
using ConsoleUI.IO;

namespace ConsoleApp.Pages
{
    public class Login : Page
    {
        public Login(Program program) : base("Login", program)
        {
        }

        public override void Display()
        {
            while (true)
            {
                Console.Clear();
                base.Display();
                var email = Input.ReadEmail("Email: ");
                var password = Input.ReadPassword("Password: ");
                var loginModel = new LoginModel
                {
                    Email = email,
                    Password = password
                };
                try
                {
                    AuthenticationService.Login(loginModel).Wait();
                }
                catch (AuthenticationException e)
                {
                    Output.DisplayError(e.Message);
                    Input.AnyKey();
                    continue;
                }
                catch (Exception e)
                {
                    Output.DisplayError(e.InnerException?.Message);
                    Input.AnyKey();
                    continue;
                }

                break;
            }

            Program.NavigateTo<Menu>();
        }
    }
}