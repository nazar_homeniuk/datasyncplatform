﻿using System;
using ConsoleApp.Services;
using ConsoleUI;
using ConsoleUI.Core;
using ConsoleUI.IO;
using Shared.NServiceBus;

namespace ConsoleApp.Pages
{
    public class Messaging : Page
    {
        private readonly NotificationService notificationService;

        public Messaging(Program program) : base("Messaging", program)
        {
            notificationService = (Program as MainProgram)?.NotificationService;
        }

        public override void Display()
        {
            while (true)
            {
                Console.Clear();
                base.Display();
                var message = Input.ReadString("Enter the message: ");
                notificationService.Trigger(new CustomNotificationMessage
                {
                    UserName = AuthenticationService.User.UserName,
                    AccessToken = AuthenticationService.User.AccessToken,
                    Content = message
                }).Wait();
                if (Input.ReadKey("Press [esc] to exit, any key to send one more") == ConsoleKey.Escape) break;
            }

            Program.NavigateBack();
        }
    }
}