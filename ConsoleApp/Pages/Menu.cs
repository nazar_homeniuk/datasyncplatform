﻿using ConsoleUI;
using ConsoleUI.Core;
using ConsoleUI.Core.Context;

namespace ConsoleApp.Pages
{
    public class Menu : MenuPage
    {
        public Menu(Program program) : base(
            "Main Menu",
            program,
            new Option("Profile info", () => program.NavigateTo<Profile>()),
            new Option("Change service class", () => program.NavigateTo<ChangeServiceClass>()),
            new Option("Change user name", () => program.NavigateTo<ChangeUserName>()),
            new Option("Change E-mail", () => program.NavigateTo<ChangeEmail>()),
            new Option("Send notification", () => program.NavigateTo<Messaging>()),
            new Option("Log out", () => program.NavigateBackTo<MainMenu>()))
        {
        }
    }
}
