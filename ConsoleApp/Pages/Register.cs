﻿using System;
using System.Security.Authentication;
using ConsoleApp.Enums;
using ConsoleApp.Models;
using ConsoleApp.Services;
using ConsoleUI;
using ConsoleUI.Core;
using ConsoleUI.IO;

namespace ConsoleApp.Pages
{
    public class Register : Page
    {
        public Register(Program program) : base("Register", program)
        {
        }

        public override void Display()
        {
            while (true)
            {
                Console.Clear();
                base.Display();
                var email = Input.ReadEmail("Email: ");
                var password = Input.ReadPassword("Password: ");
                Output.WriteLine("");
                var confirmPassword = Input.ReadPassword("Confirm password: ");
                Output.WriteLine("");
                var serviceClass = Input.ReadEnum<ServiceClasses>("Enter your service class: ").ToString();
                var registerModel = new RegisterModel
                {
                    Email = email,
                    ServiceClass = serviceClass,
                    Password = password,
                    ConfirmPassword = confirmPassword
                };
                try
                {
                    AuthenticationService.Register(registerModel).Wait();
                }
                catch (AuthenticationException e)
                {
                    Output.DisplayError(e.Message);
                    Input.AnyKey();
                    continue;
                }
                catch (Exception e)
                {
                    Output.DisplayError(e.Message);
                    Input.AnyKey();
                    continue;
                }

                break;
            }

            Output.DisplayInfo("Registered successfully!");
            Input.AnyKey();
            Program.NavigateBack();
        }
    }
}