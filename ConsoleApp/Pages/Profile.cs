﻿using ConsoleApp.Services;
using ConsoleUI;
using ConsoleUI.Core;
using ConsoleUI.IO;

namespace ConsoleApp.Pages
{
    public class Profile : Page
    {
        public Profile(Program program) : base("Profile", program)
        {
        }

        public override void Display()
        {
            base.Display();
            Output.WriteLine($"User name: {AuthenticationService.User.UserName}\nE-Mail: {AuthenticationService.User.Email}\nService class: {AuthenticationService.User.ServiceClass}");
            Output.DisplayInfo("Press any key to go back");
            Input.AnyKey();
            Program.NavigateBack();
        }
    }
}
