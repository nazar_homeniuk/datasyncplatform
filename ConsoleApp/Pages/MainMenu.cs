﻿using System;
using ConsoleUI;
using ConsoleUI.Core;
using ConsoleUI.Core.Context;

namespace ConsoleApp.Pages
{
    public class MainMenu : MenuPage
    {
        public MainMenu(Program program) : base(
            "Main Menu",
            program,
            new Option("Login", () => program.NavigateTo<Login>()),
            new Option("Register", () => program.NavigateTo<Register>()),
            new Option("Exit", () => Environment.Exit(0)))
        {
        }
    }
}