﻿using ConsoleApp.Services;
using ConsoleUI;
using ConsoleUI.Core;
using ConsoleUI.IO;
using Shared.NServiceBus;

namespace ConsoleApp.Pages
{
    public class ChangeEmail : Page
    {
        private readonly NotificationService notificationService;

        public ChangeEmail(Program program) : base("Change E-mail", program)
        {
            notificationService = (Program as MainProgram)?.NotificationService;
        }

        public override void Display()
        {
            base.Display();
            var oldEmail = AuthenticationService.User.Email;
            var newEmail = Input.ReadEmail("Enter new E-mail: ");
            AuthenticationService.User.Email = newEmail;
            notificationService.Trigger(new ChangeEmailMessage
            {
                AccessToken = AuthenticationService.User.AccessToken,
                UserName = AuthenticationService.User.UserName,
                NewEmail = newEmail,
                OldEmail = oldEmail
            }).Wait();
            Program.NavigateBack();
        }
    }
}
