﻿using ConsoleApp.Enums;
using ConsoleApp.Services;
using ConsoleUI;
using ConsoleUI.Core;
using ConsoleUI.IO;
using Shared.NServiceBus;

namespace ConsoleApp.Pages
{
    public class ChangeServiceClass : Page
    {
        private readonly NotificationService notificationService;

        public ChangeServiceClass(Program program) : base("Change service class", program)
        {
            notificationService = (Program as MainProgram)?.NotificationService;
        }

        public override void Display()
        {
            base.Display();
            var oldServiceClass = AuthenticationService.User.ServiceClass;
            var newServiceClass = Input.ReadEnum<ServiceClasses>("Enter the service class: ").ToString();
            AuthenticationService.User.ServiceClass = newServiceClass;
            notificationService.Trigger(new ChangeServiceClassMessage
            {
                UserName = AuthenticationService.User.UserName,
                AccessToken = AuthenticationService.User.AccessToken,
                NewServiceClass = newServiceClass,
                OldServiceClass = oldServiceClass
            }).Wait();
            Program.NavigateBack();
        }
    }
}
