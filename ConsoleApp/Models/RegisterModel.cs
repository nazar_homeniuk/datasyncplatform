﻿namespace ConsoleApp.Models
{
    public class RegisterModel
    {
        public string Email { get; set; }
        public string ServiceClass { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}