﻿using Newtonsoft.Json;

namespace ConsoleApp.Models
{
    public class LoginResponseModel
    {
        [JsonProperty("access_token")] public string AccessToken { get; set; }

        [JsonProperty("token_type")] public string TokenType { get; set; }

        [JsonProperty("expires_in")] public string ExpiresIn { get; set; }

        [JsonProperty("userName")] public string UserName { get; set; }

        [JsonProperty("serviceClass")] public string ServiceClass { get; set; }

        [JsonProperty(".issued")] public string Issued { get; set; }

        [JsonProperty(".expires")] public string Expires { get; set; }
    }
}