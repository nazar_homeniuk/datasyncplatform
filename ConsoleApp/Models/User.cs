﻿namespace ConsoleApp.Models
{
    public class User
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string AccessToken { get; set; }
        public string ServiceClass { get; set; }
    }
}