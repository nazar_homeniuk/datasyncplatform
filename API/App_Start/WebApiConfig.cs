﻿using System.Web.Http;
using Microsoft.Owin.Security.OAuth;

namespace API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}/{id}",
                new {id = RouteParameter.Optional}
            );

            config.InitializeCustomWebHooks();
            config.InitializeCustomWebHooksApis();
            config.InitializeCustomWebHooksAzureStorage();
        }
    }
}