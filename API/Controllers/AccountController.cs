﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using API.Models;
using API.Utils;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using NLog;
using Shared;
using Shared.NServiceBus;
using Shared.WebHooks;

namespace API.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private ApplicationUserManager userManager;
        private readonly ILogger logger;

        public AccountController()
        {
            logger = LogManager.GetCurrentClassLogger();
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get => userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            private set => userManager = value;
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; }

        [AllowAnonymous]
        [Route("Login")]
        public async Task<IHttpActionResult> Login(LoginBindingModel loginBindingModel)
        {
            if (!ModelState.IsValid)
            {
                logger.Warn($"Not valid login model {loginBindingModel.Email} {loginBindingModel.Password}");
                return BadRequest(ModelState);
            }

            using (var restClient = new RestAuthorizationClient())
            {
                var result = await restClient.Execute(loginBindingModel);
                if (result.IsSuccessStatusCode)
                {
                    return Redirect(Connections.ClientBaseAddress);
                }

                ModelState.AddModelError("", "Authorization failed");
            }

            return BadRequest();
        }

        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                logger.Warn($"Not valid register model {model.Email} {model.Password} {model.ConfirmPassword} {model.ServiceClass}");
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser
            {
                UserName = model.Email,
                Email = model.Email,
                ServiceClass = model.ServiceClass
            };
            var result = await UserManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                logger.Info($"Successfuly registered user: {model.Email}");
                return Ok();
            }

            var sb = new StringBuilder();
            foreach (var resultError in result.Errors)
            {
                sb.Append(resultError + " ");
            }

            logger.Error($"Failed to register the user {model.Email}. Errors: {sb}");
            return GetErrorResult(result);
        }

        [Route("ChangeUserName")]
        public async Task<IHttpActionResult> ChangeUserName(ChangeUserNameMessage newUserName)
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return NotFound();
            }

            user.UserName = newUserName.NewUserName;
            var result = await UserManager.UpdateAsync(user);
            if (!result.Succeeded)
            {
                var sb = new StringBuilder();
                foreach (var resultError in result.Errors)
                {
                    sb.Append(resultError + " ");
                }

                logger.Error($"Failed to change username by user {newUserName.UserName}. Errors: {sb}");
                return GetErrorResult(result);
            }

            logger.Debug($"Trying to trigger WebHook {FilterProvider.UserNameChangedEvent}");
            await this.NotifyAsync(FilterProvider.UserNameChangedEvent,
                new {Notification = newUserName, UserName = newUserName});
            logger.Info(
                $"User change his username from {newUserName} to {newUserName.NewUserName} by token {newUserName.AccessToken}");
            return Ok();
        }

        [Route("ChangeEmail")]
        public async Task<IHttpActionResult> ChangeEmail(ChangeEmailMessage newEmail)
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return NotFound();
            }

            user.Email = newEmail.NewEmail;
            var result = await UserManager.UpdateAsync(user);
            if (!result.Succeeded)
            {
                var sb = new StringBuilder();
                foreach (var resultError in result.Errors)
                {
                    sb.Append(resultError + " ");
                }

                logger.Error($"Failed to change email by user {newEmail.UserName}. Errors: {sb}");
                return GetErrorResult(result);
            }

            logger.Debug($"Trying to trigger WebHook {FilterProvider.EmailChangedEvent}");
            await this.NotifyAsync(FilterProvider.EmailChangedEvent, new {Notification = newEmail, user.UserName});
            logger.Info(
                $"User change his email from {newEmail.OldEmail} to {newEmail.NewEmail} by token {newEmail.AccessToken}");
            return Ok();
        }

        [Route("ChangeServiceClass")]
        public async Task<IHttpActionResult> ChangeServiceClass(ChangeServiceClassMessage newServiceClass)
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (user == null)
            {
                return NotFound();
            }

            user.ServiceClass = newServiceClass.NewServiceClass;
            var result = await UserManager.UpdateAsync(user);
            if (!result.Succeeded)
            {
                var sb = new StringBuilder();
                foreach (var resultError in result.Errors)
                {
                    sb.Append(resultError + " ");
                }

                logger.Error($"Failed to change service class by user {newServiceClass.UserName}. Errors: {sb}");
                return GetErrorResult(result);
            }

            logger.Debug($"Trying to trigger WebHook {FilterProvider.ServiceClassChangedEvent}");
            await this.NotifyAsync(FilterProvider.ServiceClassChangedEvent,
                new {Notification = newServiceClass, user.UserName});
            logger.Info(
                $"User change his service class from {newServiceClass.OldServiceClass} to {newServiceClass.NewServiceClass} by token {newServiceClass.AccessToken}");
            return Ok();
        }

        #region Helpers

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null) return InternalServerError();

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                    foreach (var error in result.Errors)
                        ModelState.AddModelError("", error);

                if (ModelState.IsValid) return BadRequest();

                return BadRequest(ModelState);
            }

            return null;
        }

        #endregion
    }
}