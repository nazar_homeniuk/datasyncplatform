﻿using System.Threading.Tasks;
using System.Web.Http;
using NLog;
using Shared.NServiceBus;
using Shared.WebHooks;

namespace API.Controllers
{
    [Authorize]
    public class NotifyController : ApiController
    {
        private readonly ILogger logger;

        public NotifyController()
        {
            logger = LogManager.GetCurrentClassLogger();
        }

        public async Task Post(CustomNotificationMessage notification)
        {
            logger.Debug($"Trying to tigger WebHook {FilterProvider.CustomNotificationEvent}");
            await this.NotifyAsync(FilterProvider.CustomNotificationEvent, new {Notification = notification});
        }
    }
}