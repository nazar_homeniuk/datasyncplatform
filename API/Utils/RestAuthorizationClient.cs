﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using API.Models;
using Newtonsoft.Json;
using NLog;
using Shared;
using Shared.WebHooks;

namespace API.Utils
{
    public class RestAuthorizationClient : IDisposable
    {
        private const string TokenEndpointUri = "token";
        private const string SuccessEndpointUri = "Subscriptions/Success";

        private readonly HttpClient apiHttpClient;
        private readonly HttpClient clientHttpClient;
        private readonly ILogger logger;

        public RestAuthorizationClient()
        {
            apiHttpClient = new HttpClient {BaseAddress = new Uri(Connections.ApiBaseAddress)};
            clientHttpClient = new HttpClient {BaseAddress = new Uri(Connections.ClientBaseAddress)};
            logger = LogManager.GetCurrentClassLogger();
        }

        public async Task<HttpResponseMessage> Execute(LoginBindingModel loginBindingModel)
        {
            logger.Info($"Trying to authorize user with {loginBindingModel.Email} email.");
            apiHttpClient.BaseAddress = new Uri(Connections.ApiBaseAddress);
            string token;
            string userName;
            string serviceClass;
            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("username", loginBindingModel.Email),
                new KeyValuePair<string, string>("password", loginBindingModel.Password),
                new KeyValuePair<string, string>("grant_type", "password")
            });
            var result = await apiHttpClient.PostAsync(TokenEndpointUri, content);
            if (result.IsSuccessStatusCode)
            {
                var resultContent = await result.Content.ReadAsStringAsync();
                LoginResponseModel model;
                try
                {
                    model = JsonConvert.DeserializeObject<LoginResponseModel>(resultContent);
                }
                catch (JsonSerializationException)
                {
                    logger.Error("Can't deserialize login responce model");
                    return new HttpResponseMessage(HttpStatusCode.Unauthorized);
                }

                token = model.AccessToken;
                userName = model.UserName;
                serviceClass = model.ServiceClass;
                logger.Info($"Successfuly authorize user {userName}. Authorization token: {token}.");
            }
            else
            {
                logger.Error($"Authorization failed with status code: {result.StatusCode}");
                return result;
            }

            logger.Info($"Trying to subscribe user {userName} by token {token}");
            var subscriptionInfo = new SubscriptionInfo
            {
                AccessToken = token,
                UserName = userName,
                ServiceClass = serviceClass
            };
            result = await clientHttpClient.PostAsJsonAsync(SuccessEndpointUri, subscriptionInfo);
            if (result.IsSuccessStatusCode)
            {
                logger.Info($"User {userName} successfuly subscribed by token {token}");
            }
            else
            {
                logger.Error($"Subscription proccess was failed with status code: {result.StatusCode}");
            }

            return result;

        }

        public void Dispose()
        {
            apiHttpClient.Dispose();
            clientHttpClient.Dispose();
        }
    }
}