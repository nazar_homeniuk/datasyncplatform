﻿using System;

namespace Client.Models.Notifications
{
    public class NotificationViewModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Content { get; set; }
        public bool IsDismissed { get; set; }
        public DateTime Date { get; set; }
    }
}