﻿using System.ComponentModel.DataAnnotations;

namespace Client.Models.Identity
{
    public class RegisterViewModel
    {
        [Required] public string Email { get; set; }

        [Required] public string Name { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Passwords do not match")]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }
    }
}