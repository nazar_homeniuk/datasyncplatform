﻿using System;

namespace Client.Models.Tickets
{
    public class TicketViewModel
    {
        public int Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ArrivalDate { get; set; }
        public string ServiceClass { get; set; }
    }
}