﻿namespace Client.Models.Subscriptions
{
    public class SubscriptionViewModel
    {
        public string PublisherUserName { get; set; }
        public string PublisherServiceClass { get; set; }
    }
}