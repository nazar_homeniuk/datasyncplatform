﻿using System.Linq;
using System.Web.Mvc;
using Client.BLL.Interfaces;

namespace Client.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(INotificationService notificationService) : base(notificationService)
        {
        }

        public ActionResult Index()
        {
            var count = NotificationService.GetAll().Select(i => i).Count(i => i.IsDismissed == false);
            ViewBag.NotificationCount = count;
            return View();
        }
    }
}