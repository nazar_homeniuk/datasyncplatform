﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Client.BLL.DTO.Identity;
using Client.BLL.Interfaces.Identity;
using Client.Models.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace Client.Controllers
{
    public class AccountController : Controller
    {
        private IUserService UserService => HttpContext.GetOwinContext().GetUserManager<IUserService>();

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        public ActionResult Register()
        {
            if (User.Identity.IsAuthenticated) return RedirectToAction("Index", "Home");

            return View();
        }

        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated) return RedirectToAction("Index", "Home");

            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel viewModel)
        {
            if (User.Identity.IsAuthenticated) return RedirectToAction("Index", "Home");

            await SetInitialDataAsync();
            if (!ModelState.IsValid) return View(viewModel);
            var userDto = new UserDto
            {
                Email = viewModel.Email,
                Password = viewModel.Password
            };
            var claim = await UserService.Authenticate(userDto);
            if (claim == null)
            {
                ModelState.AddModelError("", "Invalid login or password.");
            }
            else
            {
                AuthenticationManager.SignOut();
                AuthenticationManager.SignIn(new AuthenticationProperties
                {
                    IsPersistent = true
                }, claim);
                return RedirectToAction("Index", "Home");
            }

            return View(viewModel);
        }

        public ActionResult Logout()
        {
            if (!User.Identity.IsAuthenticated) return RedirectToAction("Login", "Account");

            AuthenticationManager.SignOut();
            return RedirectToAction("Login");
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Register(RegisterViewModel viewModel)
        {
            if (User.Identity.IsAuthenticated) return RedirectToAction("Index", "Home");

            await SetInitialDataAsync();
            if (!ModelState.IsValid) return View(viewModel);
            var userDto = new UserDto
            {
                Email = viewModel.Email,
                Password = viewModel.Password,
                UserName = viewModel.Email,
                Name = viewModel.Name,
                Role = "user"
            };
            var operationDetails = await UserService.Create(userDto);
            if (operationDetails.Succeeded) return RedirectToAction("Login");

            ModelState.AddModelError(operationDetails.Property, operationDetails.Message);

            return View(viewModel);
        }

        private async Task SetInitialDataAsync()
        {
            await UserService.SetInitialData(new UserDto
            {
                Email = "admin@admin.com",
                UserName = "Admin",
                Password = "Admin1234!",
                Name = "Admin",
                Role = "admin"
            }, new List<string> {"user", "admin"});
        }
    }
}