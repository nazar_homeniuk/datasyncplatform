﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Client.BLL.DTO;
using Client.BLL.Interfaces;
using Client.Models.Tickets;

namespace Client.Controllers
{
    [Authorize]
    public class TicketController : BaseController
    {
        private readonly ITicketService ticketService;

        public TicketController(INotificationService notificationService, ITicketService ticketService) : base(notificationService)
        {
            this.ticketService = ticketService;
        }

        public ActionResult Index()
        {
            var count = NotificationService.GetAll().Select(i => i).Count(i => i.IsDismissed == false);
            ViewBag.NotificationCount = count;
            var ticketsDto = ticketService.GetAll();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<TicketDto, TicketViewModel>())
                .CreateMapper();
            var tickets = mapper.Map<IEnumerable<TicketDto>, List<TicketViewModel>>(ticketsDto);
            return View(tickets);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var count = NotificationService.GetAll().Select(i => i).Count(i => i.IsDismissed == false);
            ViewBag.NotificationCount = count;
            return View();
        }

        [HttpPost]
        public ActionResult Create(TicketViewModel ticketViewModel)
        {
            ticketService.Create(new TicketDto
            {
                ArrivalDate = ticketViewModel.ArrivalDate,
                DepartureDate = ticketViewModel.DepartureDate,
                From = ticketViewModel.From,
                ServiceClass = ticketViewModel.ServiceClass,
                To = ticketViewModel.To
            });
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult TicketsForClient(string serviceClass)
        {
            var count = NotificationService.GetAll().Select(i => i).Count(i => i.IsDismissed == false);
            ViewBag.NotificationCount = count;
            var ticketsDto = ticketService.GetAllByClass(serviceClass);
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<TicketDto, TicketViewModel>())
                .CreateMapper();
            var tickets = mapper.Map<IEnumerable<TicketDto>, List<TicketViewModel>>(ticketsDto);
            return View("Index", tickets);
        }
    }
}