﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Client.BLL.DTO;
using Client.BLL.Interfaces;
using Client.Models.Notifications;

namespace Client.Controllers
{
    [Authorize]
    public class NotificationsController : BaseController
    {
        public NotificationsController(INotificationService notificationService) : base(notificationService)
        {
        }

        public ActionResult Index()
        {
            var notificationsDto = NotificationService.GetAll();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<NotificationDto, NotificationViewModel>())
                .CreateMapper();
            var notifications = mapper.Map<IEnumerable<NotificationDto>, List<NotificationViewModel>>(notificationsDto)
                .Select(i => i).Where(i => i.IsDismissed == false);
            ViewBag.NotificationCount = notifications.Count();
            return View(notifications);
        }

        [HttpPost]
        public ActionResult Dismiss(int? id)
        {
            NotificationService.Dismiss(id);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult DismissAll()
        {
            NotificationService.DismissAll();
            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        [HttpPost]
        public Task Receive(NotificationDto notification)
        {
            NotificationService.Add(notification);
            return Task.CompletedTask;
        }
    }
}