﻿using System.Web.Mvc;
using Client.BLL.Interfaces;

namespace Client.Controllers
{
    public abstract class BaseController : Controller
    {
        protected readonly INotificationService NotificationService;

        protected BaseController(INotificationService notificationService)
        {
            NotificationService = notificationService;
        }
    }
}