﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using Client.BLL.DTO;
using Client.BLL.Interfaces;
using Client.Models.Subscriptions;
using Client.Util;
using Microsoft.AspNet.Identity;
using Shared;
using Shared.WebHooks;

namespace Client.Controllers
{
    [Authorize]
    public class SubscriptionsController : BaseController
    {
        private readonly ISubscriptionService subscriptionService;

        public SubscriptionsController(INotificationService notificationService,
            ISubscriptionService subscriptionService) : base(notificationService)
        {
            this.subscriptionService = subscriptionService;
        }

        public ActionResult Index()
        {
            var count = NotificationService.GetAll().Select(i => i).Count(i => i.IsDismissed == false);
            ViewBag.NotificationCount = count;
            var subscriptionsDto = subscriptionService.GetAll();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<SubscriptionDto, SubscriptionViewModel>())
                .CreateMapper();
            var subscriptions = mapper.Map<IEnumerable<SubscriptionDto>, List<SubscriptionViewModel>>(subscriptionsDto);
            return View(subscriptions);
        }

        [HttpPost]
        public ActionResult Redirect()
        {
            return RedirectPermanent($"{Connections.ApiBaseAddress}Authentication/Login");
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> Success(SubscriptionInfo subscriptionInfo)
        {
            using (var restClient = new RestClient())
            {
                var result = await restClient.Subscribe(subscriptionInfo);
                if (!result.IsSuccessStatusCode) return RedirectToAction("Redirect");
                subscriptionService.Subscribe(new SubscriptionDto
                {
                    PublisherToken = subscriptionInfo.AccessToken,
                    PublisherUserName = subscriptionInfo.UserName,
                    PublisherServiceClass = subscriptionInfo.ServiceClass,
                    SubscriberId = User.Identity.GetUserId(),
                    SubscriberUserName = User.Identity.GetUserName()
                });
                return RedirectToAction("Index", "Subscriptions");
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public void Update(SubscriptionInfo subscriptionInfo)
        {
            subscriptionService.UpdateServiceClass(new SubscriptionDto
            {
                PublisherServiceClass = subscriptionInfo.ServiceClass,
                PublisherUserName = subscriptionInfo.UserName
            });
        }

        [HttpPost]
        public async Task<ActionResult> Unsubscribe(string publisherUserName)
        {
            var subscription = subscriptionService.GetByUserName(publisherUserName);
            using (var restClient = new RestClient())
            {
                var result = await restClient.Unsubscribe(subscription);
                if (!result.IsSuccessStatusCode) return RedirectToAction("Index", "Home");
                subscriptionService.Unsubscribe(subscription);
                return RedirectToAction("Index", "Subscriptions");
            }
        }
    }
}