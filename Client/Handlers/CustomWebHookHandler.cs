﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Client.BLL.DTO;
using Client.Util;
using Microsoft.AspNet.WebHooks;
using Newtonsoft.Json;
using Shared.NServiceBus;
using Shared.WebHooks;

namespace Client.Handlers
{
    public class CustomWebHookHandler : WebHookHandler
    {
        public override async Task ExecuteAsync(string generator, WebHookHandlerContext context)
        {
            var notification = context.GetDataOrDefault<CustomNotifications>().Notifications.First();
            var action = context.Actions.First();
            var notificationDto = new NotificationDto
            {
                Date = DateTime.Now,
                IsDismissed = false
            };
            switch (action)
            {
                case FilterProvider.CustomNotificationEvent:
                    var customNotificationMessage = Processing<CustomNotificationMessage>(notification);
                    notificationDto.Content = customNotificationMessage.Content;
                    break;
                case FilterProvider.EmailChangedEvent:
                    var changeEmailMessage = Processing<ChangeEmailMessage>(notification);
                    notificationDto.UserName = changeEmailMessage.UserName;
                    notificationDto.Content =
                        $"User changed his email from {changeEmailMessage.OldEmail} to {changeEmailMessage.NewEmail}";
                    break;
                case FilterProvider.UserNameChangedEvent:
                    var changeUserNameMessage = Processing<ChangeUserNameMessage>(notification);
                    notificationDto.UserName = changeUserNameMessage.UserName;
                    notificationDto.Content =
                        $"User changed his user name from {changeUserNameMessage.OldUserName} to {changeUserNameMessage.NewUserName}";
                    notificationDto.UserName = changeUserNameMessage.NewUserName;
                    break;
                case FilterProvider.ServiceClassChangedEvent:
                    var changeServiceClass = Processing<ChangeServiceClassMessage>(notification);
                    notificationDto.UserName = changeServiceClass.UserName;
                    notificationDto.Content =
                        $"User changed his service class from {changeServiceClass.OldServiceClass} to {changeServiceClass.NewServiceClass}";
                    using (var restClient = new RestClient())
                    {
                        var subscription = new SubscriptionInfo {ServiceClass = changeServiceClass.NewServiceClass, UserName = changeServiceClass.UserName};
                        await restClient.SubscriptionUpdate(subscription);
                    }
                    break;
                default:
                    return;
            }

            using (var restClient = new RestClient())
            {
                await restClient.ReceiveMessage(notificationDto);
            }
        }

        private static T Processing<T>(IDictionary<string, object> value)
        {
            var result = JsonConvert.DeserializeObject<T>(value["Notification"]
                .ToString());
            return result;
        }
    }
}