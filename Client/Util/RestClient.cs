﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Client.BLL.DTO;
using Client.Models;
using Shared;
using Shared.WebHooks;

namespace Client.Util
{
    public class RestClient : IDisposable
    {
        private static readonly string WebHookUri = $"{Connections.ClientBaseAddress}api/webhooks/incoming/custom";

        private const string WebHooksRegistrationUri = "api/webhooks/registrations";
        private const string NotificationReceiveEndpointUri = "Notifications/Receive";
        private const string SubscriptionUpdateEndpointUri = "Subscriptions/Update";

        private readonly HttpClient clientHttpClient;
        private readonly HttpClient apiHttpClient;

        public RestClient()
        {
            clientHttpClient = new HttpClient {BaseAddress = new Uri(Connections.ClientBaseAddress)};
            apiHttpClient = new HttpClient {BaseAddress = new Uri(Connections.ApiBaseAddress)};
        }

        public async Task<HttpResponseMessage> ReceiveMessage(NotificationDto notificationDto)
        {
            return await clientHttpClient.PostAsJsonAsync(NotificationReceiveEndpointUri, notificationDto);
        }

        public async Task<HttpResponseMessage> SubscriptionUpdate(SubscriptionInfo subscriptionInfo)
        {
            return await clientHttpClient.PostAsJsonAsync(SubscriptionUpdateEndpointUri, subscriptionInfo);
        }

        public async Task<HttpResponseMessage> Subscribe(SubscriptionInfo subscriptionInfo)
        {
            var webHook = new WebHookRegisterModel
            {
                Description = "ProfileWebHook",
                Secret = "12345678901234567890123456789012",
                WebHookUri = WebHookUri,
                Filters = new List<string>
                {
                    FilterProvider.CustomNotificationEvent,
                    FilterProvider.EmailChangedEvent,
                    FilterProvider.UserNameChangedEvent,
                    FilterProvider.ServiceClassChangedEvent
                }
            };
            apiHttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", subscriptionInfo.AccessToken);
            return await apiHttpClient.PostAsJsonAsync(WebHooksRegistrationUri, webHook);
        }

        public async Task<HttpResponseMessage> Unsubscribe(SubscriptionDto subscriptionDto)
        {
            apiHttpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("bearer", subscriptionDto.PublisherToken);
            return await apiHttpClient.DeleteAsync(WebHooksRegistrationUri);
        }

        public void Dispose()
        {
            clientHttpClient.Dispose();
            apiHttpClient.Dispose();
        }
    }
}