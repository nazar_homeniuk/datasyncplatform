﻿using Client.BLL.Interfaces;
using Client.BLL.Services;
using Ninject.Modules;

namespace Client.Util
{
    public class NotificationModule : NinjectModule
    {
        public override void Load()
        {
            Bind<INotificationService>().To<NotificationService>();
        }
    }
}