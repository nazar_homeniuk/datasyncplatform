﻿using Client.BLL.Interfaces;
using Client.BLL.Services;
using Ninject.Modules;

namespace Client.Util
{
    public class SubscriptionModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ISubscriptionService>().To<SubscriptionService>();
        }
    }
}