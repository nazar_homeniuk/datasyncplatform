﻿using Client.BLL.Interfaces;
using Client.BLL.Services;
using Ninject.Modules;

namespace Client.Util
{
    public class TicketModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ITicketService>().To<TicketService>();
        }
    }
}