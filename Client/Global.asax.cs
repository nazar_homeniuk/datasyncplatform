﻿using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Client.BLL.Infrastructure;
using Client.Util;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;

namespace Client
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            NinjectModule subscriptionModule = new SubscriptionModule();
            NinjectModule notificationModule = new NotificationModule();
            NinjectModule ticketModule = new TicketModule();
            NinjectModule serviceModule = new ServiceModule("NotificationDb");
            var kernel = new StandardKernel(notificationModule, serviceModule, subscriptionModule, ticketModule);
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
        }
    }
}