﻿using Client.DAL.Entities.Identity;
using Microsoft.AspNet.Identity;

namespace Client.DAL.Identity
{
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store) : base(store)
        {
        }
    }
}