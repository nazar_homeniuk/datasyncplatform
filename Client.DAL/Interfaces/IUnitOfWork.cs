﻿using System;
using Client.DAL.Entities;

namespace Client.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Notification> Notifications { get; }
        IRepository<Subscription> Subscriptions { get; }
        IRepository<Ticket> Tickets { get; }
        void Save();
    }
}