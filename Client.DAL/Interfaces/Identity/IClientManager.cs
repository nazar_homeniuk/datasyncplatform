﻿using System;
using Client.DAL.Entities.Identity;

namespace Client.DAL.Interfaces.Identity
{
    public interface IClientManager : IDisposable
    {
        void Create(ClientProfile item);
    }
}