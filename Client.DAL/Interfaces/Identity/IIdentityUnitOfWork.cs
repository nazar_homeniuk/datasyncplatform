﻿using System;
using System.Threading.Tasks;
using Client.DAL.Identity;

namespace Client.DAL.Interfaces.Identity
{
    public interface IIdentityUnitOfWork : IDisposable
    {
        ApplicationUserManager UserManager { get; }
        IClientManager ClientManager { get; }
        ApplicationRoleManager RoleManager { get; }
        Task SaveAsync();
    }
}