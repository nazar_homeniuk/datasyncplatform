﻿using System.Data.Entity;
using Client.DAL.Entities;

namespace Client.DAL.EF
{
    public class ApplicationContext : DbContext
    {
        static ApplicationContext()
        {
            Database.SetInitializer(new ClientDbInitializer());
        }

        public ApplicationContext(string connectionString) : base(connectionString)
        {
        }

        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<Subscription> Subscriptions { get; set; }
        public virtual DbSet<Ticket> Tickets { get; set; }
    }

    public class ClientDbInitializer : DropCreateDatabaseIfModelChanges<ApplicationContext>
    {
        protected override void Seed(ApplicationContext db)
        {
        }
    }
}