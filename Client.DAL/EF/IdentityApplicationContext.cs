﻿using System.Data.Entity;
using Client.DAL.Entities.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Client.DAL.EF
{
    public class IdentityApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public IdentityApplicationContext(string connectionString) : base(connectionString)
        {
        }

        public DbSet<ClientProfile> ClientProfiles { get; set; }
    }
}