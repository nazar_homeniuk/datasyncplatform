﻿using System;

namespace Client.DAL.Entities
{
    public class Notification
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public bool IsDismissed { get; set; }
    }
}