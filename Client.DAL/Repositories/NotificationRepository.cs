﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Client.DAL.EF;
using Client.DAL.Entities;
using Client.DAL.Interfaces;

namespace Client.DAL.Repositories
{
    public class NotificationRepository : IRepository<Notification>
    {
        private readonly ApplicationContext db;

        public NotificationRepository(ApplicationContext context)
        {
            db = context;
        }

        public IEnumerable<Notification> GetAll()
        {
            return db.Notifications;
        }

        public Notification Get(int id)
        {
            return db.Notifications.Find(id);
        }

        public Notification Create(Notification notification)
        {
            return db.Notifications.Add(notification);
        }

        public void Update(Notification notification)
        {
            db.Entry(notification).State = EntityState.Modified;
        }

        public IEnumerable<Notification> Find(Func<Notification, bool> predicate)
        {
            return db.Notifications.Where(predicate).ToList();
        }

        public void Delete(int id)
        {
            var notification = db.Notifications.Find(id);
            if (notification != null) db.Notifications.Remove(notification);
        }
    }
}