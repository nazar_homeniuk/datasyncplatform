﻿using System;
using Client.DAL.EF;
using Client.DAL.Entities;
using Client.DAL.Interfaces;

namespace Client.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext db;

        private bool disposed;

        private NotificationRepository notificationRepository;
        private SubscriptionRepository subscriptionRepository;
        private TicketRepository ticketRepository;

        public UnitOfWork(string connectionString)
        {
            db = new ApplicationContext(connectionString);
        }

        public IRepository<Notification> Notifications =>
            notificationRepository ?? (notificationRepository = new NotificationRepository(db));

        public IRepository<Subscription> Subscriptions =>
            subscriptionRepository ?? (subscriptionRepository = new SubscriptionRepository(db));

        public IRepository<Ticket> Tickets =>
            ticketRepository ?? (ticketRepository = new TicketRepository(db));

        public void Save()
        {
            db.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing) db.Dispose();

            disposed = true;
        }
    }
}