﻿using Client.DAL.EF;
using Client.DAL.Entities.Identity;
using Client.DAL.Interfaces.Identity;

namespace Client.DAL.Repositories.Identity
{
    public class ClientManager : IClientManager
    {
        public ClientManager(IdentityApplicationContext db)
        {
            Database = db;
        }

        public IdentityApplicationContext Database { get; set; }

        public void Create(ClientProfile item)
        {
            Database.ClientProfiles.Add(item);
            Database.SaveChanges();
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}