﻿using System;
using System.Threading.Tasks;
using Client.DAL.EF;
using Client.DAL.Entities.Identity;
using Client.DAL.Identity;
using Client.DAL.Interfaces.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Client.DAL.Repositories.Identity
{
    public class IdentityUnitOfWork : IIdentityUnitOfWork
    {
        private readonly IdentityApplicationContext db;

        private bool disposed;

        public IdentityUnitOfWork(string connectionString)
        {
            db = new IdentityApplicationContext(connectionString);
            UserManager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
            RoleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(db));
            ClientManager = new ClientManager(db);
        }

        public ApplicationUserManager UserManager { get; }

        public IClientManager ClientManager { get; }

        public ApplicationRoleManager RoleManager { get; }

        public async Task SaveAsync()
        {
            await db.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                UserManager.Dispose();
                RoleManager.Dispose();
                ClientManager.Dispose();
            }

            disposed = true;
        }
    }
}