﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Client.DAL.EF;
using Client.DAL.Entities;
using Client.DAL.Interfaces;

namespace Client.DAL.Repositories
{
    public class TicketRepository : IRepository<Ticket>
    {
        private readonly ApplicationContext db;

        public TicketRepository(ApplicationContext context)
        {
            db = context;
        }

        public IEnumerable<Ticket> GetAll()
        {
            return db.Tickets;
        }

        public Ticket Get(int id)
        {
            return db.Tickets.Find(id);
        }

        public Ticket Create(Ticket ticket)
        {
            return db.Tickets.Add(ticket);
        }

        public void Update(Ticket ticket)
        {
            db.Entry(ticket).State = EntityState.Modified;
        }

        public IEnumerable<Ticket> Find(Func<Ticket, bool> predicate)
        {
            return db.Tickets.Where(predicate).ToList();
        }

        public void Delete(int id)
        {
            var ticket = db.Tickets.Find(id);
            if (ticket != null) db.Tickets.Remove(ticket);
        }
    }
}
