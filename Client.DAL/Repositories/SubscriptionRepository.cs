﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Client.DAL.EF;
using Client.DAL.Entities;
using Client.DAL.Interfaces;

namespace Client.DAL.Repositories
{
    public class SubscriptionRepository : IRepository<Subscription>
    {
        private readonly ApplicationContext db;

        public SubscriptionRepository(ApplicationContext context)
        {
            db = context;
        }

        public IEnumerable<Subscription> GetAll()
        {
            return db.Subscriptions;
        }

        public Subscription Get(int id)
        {
            return db.Subscriptions.Find(id);
        }

        public Subscription Create(Subscription subscription)
        {
            return db.Subscriptions.Add(subscription);
        }

        public void Update(Subscription subscription)
        {
            db.Entry(subscription).State = EntityState.Modified;
        }

        public IEnumerable<Subscription> Find(Func<Subscription, bool> predicate)
        {
            return db.Subscriptions.Where(predicate).ToList();
        }

        public void Delete(int id)
        {
            var subscription = db.Subscriptions.Find(id);
            if (subscription != null) db.Subscriptions.Remove(subscription);
        }
    }
}