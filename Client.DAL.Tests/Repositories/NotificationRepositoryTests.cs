﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Client.DAL.EF;
using Client.DAL.Entities;
using Client.DAL.Repositories;
using Moq;
using NUnit.Framework;

namespace Client.DAL.Tests.Repositories
{
    [TestFixture]
    public class NotificationRepositoryTests
    {
        private List<Notification> notificationsData;
        private Mock<ApplicationContext> contextMock;
        private Mock<DbSet<Notification>> setMock;

        [SetUp]
        public void SetUp()
        {
            notificationsData = new List<Notification>
            {
                new Notification
                    {Content = "TEST", Date = DateTime.MaxValue, Id = 0, IsDismissed = false, UserName = "TEST"},
                new Notification
                    {Content = "TEST2", Date = DateTime.MaxValue, Id = 1, IsDismissed = false, UserName = "TEST2"},
                new Notification
                    {Content = "TEST3", Date = DateTime.MaxValue, Id = 2, IsDismissed = false, UserName = "TEST3"}
            };

            var queryable = notificationsData.AsQueryable();
            setMock = new Mock<DbSet<Notification>>();
            setMock.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => notificationsData.FirstOrDefault(d => d.Id == (int)ids[0]));
            setMock.As<IQueryable<Notification>>().Setup(m => m.Provider).Returns(queryable.Provider);
            setMock.As<IQueryable<Notification>>().Setup(m => m.Expression).Returns(queryable.Expression);
            setMock.As<IQueryable<Notification>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            setMock.As<IQueryable<Notification>>().Setup(m => m.GetEnumerator()).Returns(queryable.GetEnumerator());

            contextMock = new Mock<ApplicationContext>("DefaultConnection");
            contextMock.Setup(a => a.Notifications).Returns(setMock.Object);
        }

        [Test]
        public void GetAll()
        {
            var notificationRepository = new NotificationRepository(contextMock.Object);

            var notifications = notificationRepository.GetAll();

            contextMock.VerifyGet(m => m.Notifications, Times.Once);
            Assert.IsNotNull(notifications);
            Assert.AreEqual(notifications.Count(), 3);
        }

        [Test]
        public void Get()
        {
            var notificationRepository = new NotificationRepository(contextMock.Object);

            var notification = notificationRepository.Get(0);

            Assert.IsNotNull(notification);
            Assert.AreEqual(notification.Id, 0);
        }

        [Test]
        public void Get_When_Value_Not_Exists()
        {
            var notificationRepository = new NotificationRepository(contextMock.Object);

            var notification = notificationRepository.Get(10);

            Assert.IsNull(notification);
        }

        [Test]
        public void Create()
        {
            var notificationRepository = new NotificationRepository(contextMock.Object);
            var notification = new Notification
                {Content = "TEST", Date = DateTime.MaxValue, Id = 4, IsDismissed = false, UserName = "TEST"};

            notificationRepository.Create(notification);

            setMock.Verify(m => m.Add(It.Is<Notification>(n => n.Id == notification.Id)), Times.Once);
        }

        [Test]
        public void Create_When_Value_Already_Exists()
        {
            var notificationRepository = new NotificationRepository(contextMock.Object);
            var notification = new Notification
                { Content = "TEST", Date = DateTime.MaxValue, Id = 0, IsDismissed = false, UserName = "TEST" };

            var returnValue = notificationRepository.Create(notification);

            setMock.Verify(m => m.Add(It.Is<Notification>(n => n.Id == notification.Id)));
            Assert.IsNull(returnValue);
        }

        [Test]
        public void Find()
        {
            var notificationRepository = new NotificationRepository(contextMock.Object);

            var notifications = notificationRepository.Find(i => i.Content == "TEST");

            Assert.AreEqual(notifications.Count(), 1);
        }

        [Test]
        public void Delete()
        {
            var notificationRepository = new NotificationRepository(contextMock.Object);

            notificationRepository.Delete(0);

            setMock.Verify(m => m.Find(It.Is<int>(i => i == 0)), Times.Once);
            setMock.Verify(m => m.Remove(It.IsAny<Notification>()), Times.Once);
        }

        [Test]
        public void Delete_If_Not_Exists()
        {
            var notificationRepository = new NotificationRepository(contextMock.Object);

            notificationRepository.Delete(10);

            setMock.Verify(m => m.Find(It.Is<int>(i => i == 10)), Times.Once);
            setMock.Verify(m => m.Remove(It.IsAny<Notification>()), Times.Never);
        }

    }
}
