﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Client.DAL.EF;
using Client.DAL.Entities;
using Client.DAL.Repositories;
using Moq;
using NUnit.Framework;

namespace Client.DAL.Tests.Repositories
{
    [TestFixture]
    public class SubscriptionRepositoryTests
    {
        private List<Subscription> subscriptionsData;
        private Mock<ApplicationContext> contextMock;
        private Mock<DbSet<Subscription>> setMock;

        [SetUp]
        public void SetUp()
        {
            subscriptionsData = new List<Subscription>
            {
                new Subscription
                    {Id = 0, PublisherServiceClass = "TEST", PublisherToken = "TEST", PublisherUserName = "TEST", SubscriberId = "TEST", SubscriberUserName = "TEST"},
                new Subscription
                    {Id = 1, PublisherServiceClass = "TEST2", PublisherToken = "TEST2", PublisherUserName = "TEST2", SubscriberId = "TEST2", SubscriberUserName = "TEST2"},
                new Subscription
                    {Id = 2, PublisherServiceClass = "TEST3", PublisherToken = "TEST3", PublisherUserName = "TEST3", SubscriberId = "TEST3", SubscriberUserName = "TEST3"}
            };

            var queryable = subscriptionsData.AsQueryable();
            setMock = new Mock<DbSet<Subscription>>();
            setMock.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => subscriptionsData.FirstOrDefault(d => d.Id == (int)ids[0]));
            setMock.As<IQueryable<Subscription>>().Setup(m => m.Provider).Returns(queryable.Provider);
            setMock.As<IQueryable<Subscription>>().Setup(m => m.Expression).Returns(queryable.Expression);
            setMock.As<IQueryable<Subscription>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            setMock.As<IQueryable<Subscription>>().Setup(m => m.GetEnumerator()).Returns(queryable.GetEnumerator());

            contextMock = new Mock<ApplicationContext>("DefaultConnection");
            contextMock.Setup(a => a.Subscriptions).Returns(setMock.Object);
        }

        [Test]
        public void GetAll()
        {
            var subscriptionRepository = new SubscriptionRepository(contextMock.Object);

            var subscriptions = subscriptionRepository.GetAll();

            contextMock.VerifyGet(m => m.Subscriptions, Times.Once);
            Assert.IsNotNull(subscriptions);
            Assert.AreEqual(subscriptions.Count(), 3);
        }

        [Test]
        public void Get()
        {
            var subscriptionRepository = new SubscriptionRepository(contextMock.Object);

            var subscriptions = subscriptionRepository.Get(0);

            Assert.IsNotNull(subscriptions);
            Assert.AreEqual(subscriptions.Id, 0);
        }

        [Test]
        public void Get_When_Value_Not_Exists()
        {
            var subscriptionRepository = new SubscriptionRepository(contextMock.Object);

            var subscriptions = subscriptionRepository.Get(10);

            Assert.IsNull(subscriptions);
        }

        [Test]
        public void Create()
        {
            var subscriptionRepository = new SubscriptionRepository(contextMock.Object);
            var subscription = new Subscription
            {
                Id = 4, PublisherServiceClass = "TEST",
                PublisherToken = "TEST",
                PublisherUserName = "TEST",
                SubscriberId = "TEST",
                SubscriberUserName = "TEST"
            };

            subscriptionRepository.Create(subscription);

            setMock.Verify(m => m.Add(It.Is<Subscription>(n => n.Id == subscription.Id)), Times.Once);
        }

        [Test]
        public void Create_When_Value_Already_Exists()
        {
            var subscriptionRepository = new SubscriptionRepository(contextMock.Object);
            var subscription = new Subscription
            {
                Id = 0,
                PublisherServiceClass = "TEST",
                PublisherToken = "TEST",
                PublisherUserName = "TEST",
                SubscriberId = "TEST",
                SubscriberUserName = "TEST"
            };

            var returnValue = subscriptionRepository.Create(subscription);

            setMock.Verify(m => m.Add(It.Is<Subscription>(n => n.Id == subscription.Id)));
            Assert.IsNull(returnValue);
        }

        [Test]
        public void Find()
        {
            var subscriptionRepository = new SubscriptionRepository(contextMock.Object);

            var subscription = subscriptionRepository.Find(i => i.PublisherUserName == "TEST");

            Assert.AreEqual(subscription.Count(), 1);
        }

        [Test]
        public void Delete()
        {
            var subscriptionRepository = new SubscriptionRepository(contextMock.Object);

            subscriptionRepository.Delete(0);

            setMock.Verify(m => m.Find(It.Is<int>(i => i == 0)), Times.Once);
            setMock.Verify(m => m.Remove(It.IsAny<Subscription>()), Times.Once);
        }

        [Test]
        public void Delete_If_Not_Exists()
        {
            var subscriptionRepository = new SubscriptionRepository(contextMock.Object);

            subscriptionRepository.Delete(10);

            setMock.Verify(m => m.Find(It.Is<int>(i => i == 10)), Times.Once);
            setMock.Verify(m => m.Remove(It.IsAny<Subscription>()), Times.Never);
        }

    }
}
