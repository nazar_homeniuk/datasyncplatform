﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Client.DAL.EF;
using Client.DAL.Entities;
using Client.DAL.Repositories;
using Moq;
using NUnit.Framework;

namespace Client.DAL.Tests.Repositories
{
    [TestFixture]
    public class TicketRepositoryTests
    {
        private List<Ticket> ticketsData;
        private Mock<ApplicationContext> contextMock;
        private Mock<DbSet<Ticket>> setMock;

        [SetUp]
        public void SetUp()
        {
            ticketsData = new List<Ticket>
            {
                new Ticket
                    {Id = 0, From = "TEST", To = "TEST", ArrivalDate = DateTime.MaxValue, DepartureDate = DateTime.MaxValue, ServiceClass = "TEST"},
                new Ticket
                    {Id = 1, From = "TEST2", To = "TEST2", ArrivalDate = DateTime.MaxValue, DepartureDate = DateTime.MaxValue, ServiceClass = "TEST2"},
                new Ticket
                    {Id = 2, From = "TEST3", To = "TEST3", ArrivalDate = DateTime.MaxValue, DepartureDate = DateTime.MaxValue, ServiceClass = "TEST3"}
            };

            var queryable = ticketsData.AsQueryable();
            setMock = new Mock<DbSet<Ticket>>();
            setMock.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => ticketsData.FirstOrDefault(d => d.Id == (int)ids[0]));
            setMock.As<IQueryable<Ticket>>().Setup(m => m.Provider).Returns(queryable.Provider);
            setMock.As<IQueryable<Ticket>>().Setup(m => m.Expression).Returns(queryable.Expression);
            setMock.As<IQueryable<Ticket>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            setMock.As<IQueryable<Ticket>>().Setup(m => m.GetEnumerator()).Returns(queryable.GetEnumerator());

            contextMock = new Mock<ApplicationContext>("DefaultConnection");
            contextMock.Setup(a => a.Tickets).Returns(setMock.Object);
        }

        [Test]
        public void GetAll()
        {
            var ticketRepository = new TicketRepository(contextMock.Object);

            var tickets = ticketRepository.GetAll();

            contextMock.VerifyGet(m => m.Tickets, Times.Once);
            Assert.IsNotNull(tickets);
            Assert.AreEqual(tickets.Count(), 3);
        }

        [Test]
        public void Get()
        {
            var ticketRepository = new TicketRepository(contextMock.Object);

            var tickets = ticketRepository.Get(0);

            Assert.IsNotNull(tickets);
            Assert.AreEqual(tickets.Id, 0);
        }

        [Test]
        public void Get_When_Value_Not_Exists()
        {
            var ticketRepository = new TicketRepository(contextMock.Object);

            var tickets = ticketRepository.Get(10);

            Assert.IsNull(tickets);
        }

        [Test]
        public void Create()
        {
            var ticketRepository = new TicketRepository(contextMock.Object);
            var ticket = new Ticket
            {
                Id = 4,
                From = "TEST",
                To = "TEST",
                ArrivalDate = DateTime.MaxValue,
                DepartureDate = DateTime.MaxValue,
                ServiceClass = "TEST"
            };

            ticketRepository.Create(ticket);

            setMock.Verify(m => m.Add(It.Is<Ticket>(n => n.Id == ticket.Id)), Times.Once);
        }

        [Test]
        public void Create_When_Value_Already_Exists()
        {
            var ticketRepository = new TicketRepository(contextMock.Object);
            var ticket = new Ticket
            {
                Id = 0,
                From = "TEST",
                To = "TEST",
                ArrivalDate = DateTime.MaxValue,
                DepartureDate = DateTime.MaxValue,
                ServiceClass = "TEST"
            };

            var returnValue = ticketRepository.Create(ticket);

            setMock.Verify(m => m.Add(It.Is<Ticket>(n => n.Id == ticket.Id)));
            Assert.IsNull(returnValue);
        }

        [Test]
        public void Find()
        {
            var ticketRepository = new TicketRepository(contextMock.Object);

            var tickets = ticketRepository.Find(i => i.From == "TEST");

            Assert.AreEqual(tickets.Count(), 1);
        }

        [Test]
        public void Delete()
        {
            var ticketRepository = new TicketRepository(contextMock.Object);

            ticketRepository.Delete(0);

            setMock.Verify(m => m.Find(It.Is<int>(i => i == 0)), Times.Once);
            setMock.Verify(m => m.Remove(It.IsAny<Ticket>()), Times.Once);
        }

        [Test]
        public void Delete_If_Not_Exists()
        {
            var ticketRepository = new TicketRepository(contextMock.Object);

            ticketRepository.Delete(10);

            setMock.Verify(m => m.Find(It.Is<int>(i => i == 10)), Times.Once);
            setMock.Verify(m => m.Remove(It.IsAny<Ticket>()), Times.Never);
        }

    }
}
