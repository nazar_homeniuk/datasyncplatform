﻿namespace Shared
{
    public static class Connections
    {
        public const string ClientBaseAddress = "http://localhost:63490/";
        public const string ApiBaseAddress = "http://localhost:57344/";
    }
}