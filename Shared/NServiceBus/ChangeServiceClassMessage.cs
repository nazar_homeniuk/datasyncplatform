﻿using Shared.NServiceBus.Interfaces;

namespace Shared.NServiceBus
{
    public class ChangeServiceClassMessage : INotificationMessage
    {
        public string AccessToken { get; set; }
        public string UserName { get; set; }
        public string OldServiceClass { get; set; }
        public string NewServiceClass { get; set; }
    }
}
