﻿using Shared.NServiceBus.Interfaces;

namespace Shared.NServiceBus
{
    public class ChangeUserNameMessage : INotificationMessage
    {
        public string AccessToken { get; set; }
        public string UserName { get; set; }
        public string OldUserName { get; set; }
        public string NewUserName { get; set; }
    }
}
