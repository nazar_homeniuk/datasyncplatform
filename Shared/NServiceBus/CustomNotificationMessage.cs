﻿using Shared.NServiceBus.Interfaces;

namespace Shared.NServiceBus
{
    public class CustomNotificationMessage : INotificationMessage
    {
        public string AccessToken { get; set; }
        public string UserName { get; set; }
        public string Content { get; set; }
    }
}