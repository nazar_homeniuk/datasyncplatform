﻿using NServiceBus;
using Shared.NServiceBus.Interfaces;

namespace Shared.NServiceBus
{
    public class ChangeEmailMessage : INotificationMessage
    {
        public string AccessToken { get; set; }
        public string UserName { get; set; }
        public string OldEmail { get; set; }
        public string NewEmail { get; set; }
    }
}
