﻿using NServiceBus;

namespace Shared.NServiceBus.Interfaces
{
    public interface INotificationMessage : IMessage
    {
        string AccessToken { get; set; }
        string UserName { get; set; }
    }
}