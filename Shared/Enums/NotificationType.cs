﻿namespace Shared.Enums
{
    public enum NotificationType
    {
        ChangeEmail,
        ChangeUserName,
        ChangeServiceClass,
        Custom
    }
}