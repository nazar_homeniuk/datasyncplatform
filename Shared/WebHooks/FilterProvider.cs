﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Microsoft.AspNet.WebHooks;

namespace Shared.WebHooks
{
    public class FilterProvider : IWebHookFilterProvider
    {
        public const string UserNameChangedEvent = "UserNameChangedEvent";
        public const string EmailChangedEvent = "EmailChangedEvent";
        public const string ServiceClassChangedEvent = "ServiceClassChangedEvent";
        public const string CustomNotificationEvent = "CustomNotificationEvent";

        private readonly Collection<WebHookFilter> filters;

        public FilterProvider()
        {
            filters = new Collection<WebHookFilter>
            {
                new WebHookFilter
                    {Name = UserNameChangedEvent, Description = "Triggered when user changed his user name"},
                new WebHookFilter
                    {Name = EmailChangedEvent, Description = "Triggered when user changed his E-Mail"},
                new WebHookFilter
                    {Name = CustomNotificationEvent, Description = "Triggered when user sends notification with custom content"},
                new WebHookFilter
                    {Name = ServiceClassChangedEvent, Description = "Triggered when user changed his Service class"}
            };
        }

        public Task<Collection<WebHookFilter>> GetFiltersAsync()
        {
            return Task.FromResult(filters);
        }
    }
}
