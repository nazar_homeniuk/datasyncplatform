﻿namespace Shared.WebHooks
{
    public class SubscriptionInfo
    {
        public string AccessToken { get; set; }
        public string UserName { get; set; }
        public string ServiceClass { get; set; }
    }
}
