﻿using System;
using System.Collections.Generic;
using System.Linq;
using Client.BLL.DTO;
using Client.BLL.Services;
using Client.DAL.Entities;
using Client.DAL.Interfaces;
using Moq;
using NUnit.Framework;

namespace Client.BLL.Tests.Services
{
    [TestFixture]
    public class NotificationServiceTests
    {
        private Mock<IUnitOfWork> unitOfWorkMock;
        private Mock<IRepository<Notification>> repositoryMock;

        [SetUp]
        public void SetUp()
        {
            unitOfWorkMock = new Mock<IUnitOfWork>();
            repositoryMock = new Mock<IRepository<Notification>>();
            unitOfWorkMock.Setup(m => m.Notifications).Returns(repositoryMock.Object);
        }

        [Test]
        public void Add()
        {
            var notificationService = new NotificationService(unitOfWorkMock.Object);
            var notificationDto = new NotificationDto
            {
                Content = "TEST",
                Date = DateTime.MaxValue,
                Id = 0,
                IsDismissed = false,
                UserName = "TEST"
            };

            notificationService.Add(notificationDto);

            repositoryMock.Verify(m => m.Create(It.Is<Notification>(n =>
                n.Content == notificationDto.Content && 
                n.IsDismissed == notificationDto.IsDismissed &&
                n.UserName == notificationDto.UserName && 
                n.Id == notificationDto.Id &&
                n.Date == notificationDto.Date)), Times.Once);
            unitOfWorkMock.Verify(n => n.Save(), Times.Once);
        }

        [Test]
        public void Dismiss()
        {
            var notificationService = new NotificationService(unitOfWorkMock.Object);
            var notification = new Notification
            {
                Content = "TEST",
                Date = DateTime.MaxValue,
                Id = 0,
                IsDismissed = false,
                UserName = "TEST"
            };
            repositoryMock.Setup(m => m.Get(It.IsAny<int>())).Returns(notification);

            notificationService.Dismiss(0);
            
            Assert.IsTrue(notification.IsDismissed);
            repositoryMock.Verify(m => m.Update(It.Is<Notification>(n =>
                n.Content == notification.Content &&
                n.IsDismissed == notification.IsDismissed &&
                n.UserName == notification.UserName &&
                n.Id == notification.Id &&
                n.Date == notification.Date)), Times.Once);
            unitOfWorkMock.Verify(n => n.Save(), Times.Once);
        }

        [Test]
        public void Dismiss_When_Id_Is_Null()
        {
            var notificationService = new NotificationService(unitOfWorkMock.Object);

            Assert.Throws<ArgumentNullException>(() => notificationService.Dismiss(null));
        }

        [Test]
        public void Dismiss_When_Notification_Not_Exists()
        {
            var notificationService = new NotificationService(unitOfWorkMock.Object);
            repositoryMock.Setup(m => m.Get(It.IsAny<int>())).Returns((Notification) null);

            Assert.Throws<InvalidOperationException>(() => notificationService.Dismiss(0));
        }

        [Test]
        public void DismissAll()
        {
            var notificationService = new NotificationService(unitOfWorkMock.Object);
            var notifications = new List<Notification>
            {
                new Notification
                {
                    Content = "TEST",
                    Date = DateTime.MaxValue,
                    Id = 0,
                    IsDismissed = false,
                    UserName = "TEST"
                },
                new Notification
                {
                    Content = "TEST",
                    Date = DateTime.MaxValue,
                    Id = 1,
                    IsDismissed = false,
                    UserName = "TEST"
                },
                new Notification
                {
                    Content = "TEST",
                    Date = DateTime.MaxValue,
                    Id = 2,
                    IsDismissed = false,
                    UserName = "TEST"
                }
            };
            repositoryMock.Setup(m => m.Find(It.IsAny<Func<Notification, bool>>())).Returns(notifications);

            notificationService.DismissAll();

            foreach (var notification in notifications)
            {
                Assert.IsTrue(notification.IsDismissed);
            }

            repositoryMock.Verify(m => m.Update(It.IsAny<Notification>()), Times.Exactly(3));
            unitOfWorkMock.Verify(m => m.Save(), Times.Once);
        }

        [Test]
        public void Dismiss_When_Notifications_Not_Exists()
        {
            var notificationService = new NotificationService(unitOfWorkMock.Object);
            repositoryMock.Setup(m => m.Find(It.IsAny<Func<Notification, bool>>())).Returns(new List<Notification>());

            notificationService.DismissAll();

            repositoryMock.Verify(m => m.Update(It.IsAny<Notification>()), Times.Never);
            unitOfWorkMock.Verify(m => m.Save(), Times.Never);
        }

        [Test]
        public void Get()
        {
            var notificationService = new NotificationService(unitOfWorkMock.Object);
            var notification = new Notification
            {
                Content = "TEST",
                Date = DateTime.MaxValue,
                Id = 0,
                IsDismissed = false,
                UserName = "TEST"
            };
            repositoryMock.Setup(m => m.Get(It.IsAny<int>())).Returns(notification);

            var notificationDto = notificationService.Get(0);

            Assert.AreEqual(notification.Id, notificationDto.Id);
            Assert.AreEqual(notification.UserName, notificationDto.UserName);
            Assert.AreEqual(notification.Content, notificationDto.Content);
            Assert.AreEqual(notification.Date, notificationDto.Date);
            Assert.AreEqual(notification.IsDismissed, notificationDto.IsDismissed);
        }

        [Test]
        public void Get_When_Id_Is_Null()
        {
            var notificationService = new NotificationService(unitOfWorkMock.Object);

            Assert.Throws<ArgumentNullException>(() => notificationService.Get(null));
        }

        [Test]
        public void Get_When_Notification_Not_Exists()
        {
            var notificationService = new NotificationService(unitOfWorkMock.Object);
            repositoryMock.Setup(m => m.Get(It.IsAny<int>())).Returns((Notification) null);

            Assert.Throws<InvalidOperationException>(() => notificationService.Get(0));
        }

        [Test]
        public void GetAll()
        {
            var notificationService = new NotificationService(unitOfWorkMock.Object);
            var notifications = new List<Notification>
            {
                new Notification
                {
                    Content = "TEST",
                    Date = DateTime.MaxValue,
                    Id = 0,
                    IsDismissed = false,
                    UserName = "TEST"
                },
                new Notification
                {
                    Content = "TEST",
                    Date = DateTime.MaxValue,
                    Id = 1,
                    IsDismissed = false,
                    UserName = "TEST"
                },
                new Notification
                {
                    Content = "TEST",
                    Date = DateTime.MaxValue,
                    Id = 2,
                    IsDismissed = false,
                    UserName = "TEST"
                }
            };
            repositoryMock.Setup(m => m.GetAll()).Returns(notifications);

            var notificationsDto = notificationService.GetAll();

            repositoryMock.Verify(m => m.GetAll(), Times.Once);
            Assert.AreEqual(notificationsDto.Count(), 3);
        }

        [Test]
        public void Dispose()
        {
            var notificationService = new NotificationService(unitOfWorkMock.Object);

            notificationService.Dispose();

            unitOfWorkMock.Verify(m => m.Dispose(), Times.Once);
        }
    }
}
