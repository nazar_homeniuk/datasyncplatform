﻿using System;
using System.Collections.Generic;
using System.Linq;
using Client.BLL.DTO;
using Client.BLL.Services;
using Client.DAL.Entities;
using Client.DAL.Interfaces;
using Moq;
using NUnit.Framework;

namespace Client.BLL.Tests.Services
{
    [TestFixture]
    public class TicketServiceTests
    {
        private Mock<IUnitOfWork> unitOfWorkMock;
        private Mock<IRepository<Ticket>> repositoryMock;

        [SetUp]
        public void SetUp()
        {
            unitOfWorkMock = new Mock<IUnitOfWork>();
            repositoryMock = new Mock<IRepository<Ticket>>();
            unitOfWorkMock.Setup(m => m.Tickets).Returns(repositoryMock.Object);
        }

        [Test]
        public void Create()
        {
            var ticketService = new TicketService(unitOfWorkMock.Object);
            var ticketDto = new TicketDto
            {
                ArrivalDate = DateTime.MaxValue,
                DepartureDate = DateTime.MaxValue,
                From = "TEST",
                To = "TEST",
                ServiceClass = "TEST",
                Id = 0
            };

            ticketService.Create(ticketDto);

            repositoryMock.Verify(
                m => m.Create(It.Is<Ticket>(n =>
                    n.Id == ticketDto.Id && n.ArrivalDate == ticketDto.ArrivalDate &&
                    n.DepartureDate == ticketDto.DepartureDate && n.From == ticketDto.From && n.To == ticketDto.To &&
                    n.ServiceClass == ticketDto.ServiceClass)), Times.Once);
            unitOfWorkMock.Verify(m => m.Save(), Times.Once);
        }

        [Test]
        public void Delete()
        {
            var ticketService = new TicketService(unitOfWorkMock.Object);
            repositoryMock.Setup(m => m.Get(It.IsAny<int>())).Returns(new Ticket());

            ticketService.Delete(0);

            repositoryMock.Verify(m => m.Delete(It.Is<int>(i => i == 0)), Times.Once);
            unitOfWorkMock.Verify(m => m.Save(), Times.Once);

        }

        [Test]
        public void Delete_When_Id_Is_Null()
        {
            var ticketService = new TicketService(unitOfWorkMock.Object);
            repositoryMock.Setup(m => m.Get(It.IsAny<int>())).Returns(new Ticket());

            ticketService.Delete(null);

            repositoryMock.Verify(m => m.Delete(It.Is<int>(i => i == 0)), Times.Never);
            unitOfWorkMock.Verify(m => m.Save(), Times.Never);
        }

        [Test]
        public void Delete_When_Ticket_Not_Found()
        {
            var ticketService = new TicketService(unitOfWorkMock.Object);
            repositoryMock.Setup(m => m.Get(It.IsAny<int>())).Returns((Ticket) null);

            ticketService.Delete(0);

            repositoryMock.Verify(m => m.Delete(It.Is<int>(i => i == 0)), Times.Never);
            unitOfWorkMock.Verify(m => m.Save(), Times.Never);
        }

        [Test]
        public void GetAll()
        {
            var ticketService = new TicketService(unitOfWorkMock.Object);
            var ticket = new Ticket
            {
                ArrivalDate = DateTime.MaxValue,
                DepartureDate = DateTime.MaxValue,
                From = "TEST",
                To = "TEST",
                ServiceClass = "TEST",
                Id = 0
            };
            repositoryMock.Setup(m => m.GetAll()).Returns(new List<Ticket> {ticket, ticket, ticket});

            var ticketsDto = ticketService.GetAll();

            Assert.AreEqual(ticketsDto.Count(), 3);
        }

        [Test]
        public void Get()
        {
            var ticketService = new TicketService(unitOfWorkMock.Object);
            var ticket = new Ticket
            {
                ArrivalDate = DateTime.MaxValue,
                DepartureDate = DateTime.MaxValue,
                From = "TEST",
                To = "TEST",
                ServiceClass = "TEST",
                Id = 0
            };
            repositoryMock.Setup(m => m.Get(It.IsAny<int>())).Returns(ticket);

            var ticketDto = ticketService.Get(0);

            repositoryMock.Verify(m => m.Get(It.Is<int>(i => i == 0)));
            Assert.AreEqual(ticket.Id, ticketDto.Id);
            Assert.AreEqual(ticket.ArrivalDate, ticketDto.ArrivalDate);
            Assert.AreEqual(ticket.DepartureDate, ticketDto.DepartureDate);
            Assert.AreEqual(ticket.From, ticketDto.From);
            Assert.AreEqual(ticket.To, ticketDto.To);
            Assert.AreEqual(ticket.ServiceClass, ticketDto.ServiceClass);
        }

        [Test]
        public void Get_When_Ticket_Not_Exists()
        {
            var ticketService = new TicketService(unitOfWorkMock.Object);
            repositoryMock.Setup(m => m.Get(It.IsAny<int>())).Returns((Ticket) null);

            var ticketDto = ticketService.Get(0);

            Assert.IsNull(ticketDto);
        }

        [Test]
        public void GetAllByClass()
        {
            var ticketService = new TicketService(unitOfWorkMock.Object);
            var tickets = new List<Ticket>
            {
                new Ticket {ServiceClass = "TEST1"},
                new Ticket {ServiceClass = "TEST2"},
                new Ticket {ServiceClass = "TEST1"}
            };
            repositoryMock.Setup(m => m.GetAll()).Returns(tickets);

            var ticketsDto = ticketService.GetAllByClass("TEST1");

            Assert.AreEqual(ticketsDto.Count(), 2);
        }

        [Test]
        public void Dispose()
        {
            var ticketService = new TicketService(unitOfWorkMock.Object);

            ticketService.Dispose();

            unitOfWorkMock.Verify(m => m.Dispose(), Times.Once);
        }
    }
}
