﻿using System;
using System.Collections.Generic;
using Client.BLL.DTO;
using Client.BLL.Services;
using Client.DAL.Entities;
using Client.DAL.Interfaces;
using Moq;
using NUnit.Framework;

namespace Client.BLL.Tests.Services
{
    [TestFixture]
    public class SubscriptionServiceTests
    {
        private Mock<IUnitOfWork> unitOfWorkMock;
        private Mock<IRepository<Subscription>> repositoryMock;

        [SetUp]
        public void SetUp()
        {
            unitOfWorkMock = new Mock<IUnitOfWork>();
            repositoryMock = new Mock<IRepository<Subscription>>();
            unitOfWorkMock.Setup(m => m.Subscriptions).Returns(repositoryMock.Object);
        }

        [Test]
        public void Subscribe()
        {
            var subscriptionService = new SubscriptionService(unitOfWorkMock.Object);
            var subscriptionDto = new SubscriptionDto
            {
                Id = 0,
                PublisherServiceClass = "TEST",
                PublisherToken = "TEST",
                PublisherUserName = "TEST",
                SubscriberId = "TEST",
                SubscriberUserName = "TEST"
            };

            subscriptionService.Subscribe(subscriptionDto);

            repositoryMock.Verify(
                n => n.Create(It.Is<Subscription>(m =>
                    m.Id == subscriptionDto.Id && m.PublisherUserName == subscriptionDto.PublisherUserName &&
                    m.PublisherServiceClass == subscriptionDto.PublisherServiceClass &&
                    m.PublisherToken == subscriptionDto.PublisherToken &&
                    m.SubscriberId == subscriptionDto.SubscriberId &&
                    m.SubscriberUserName == subscriptionDto.SubscriberUserName)), Times.Once);

            unitOfWorkMock.Verify(m => m.Save(), Times.Once);
        }

        [Test]
        public void Subscribe_When_Already_Subscribed()
        {
            var subscriptionService = new SubscriptionService(unitOfWorkMock.Object);
            var subscriptionDto = new SubscriptionDto
            {
                Id = 0,
                PublisherServiceClass = "TEST",
                PublisherToken = "TEST",
                PublisherUserName = "TEST",
                SubscriberId = "TEST",
                SubscriberUserName = "TEST"
            };
            repositoryMock.Setup(m => m.Find(It.IsAny<Func<Subscription, bool>>()))
                .Returns(new List<Subscription> {new Subscription()});

            subscriptionService.Subscribe(subscriptionDto);

            repositoryMock.Verify(n => n.Create(It.IsAny<Subscription>()), Times.Never);
            unitOfWorkMock.Verify(m => m.Save(), Times.Never);
        }

        [Test]
        public void Unsubscribe()
        {
            var subscriptionService = new SubscriptionService(unitOfWorkMock.Object);
            var subscriptionDto = new SubscriptionDto
            {
                Id = 0,
                PublisherServiceClass = "TEST",
                PublisherToken = "TEST",
                PublisherUserName = "TEST",
                SubscriberId = "TEST",
                SubscriberUserName = "TEST"
            };
            repositoryMock.Setup(m => m.Get(It.IsAny<int>())).Returns(new Subscription());

            subscriptionService.Unsubscribe(subscriptionDto);

            repositoryMock.Verify(m => m.Delete(It.Is<int>(i => i == subscriptionDto.Id)), Times.Once);
            unitOfWorkMock.Verify(m => m.Save(), Times.Once);
        }

        [Test]
        public void Unsubscribe_When_Subscription_Not_Exists()
        {
            var subscriptionService = new SubscriptionService(unitOfWorkMock.Object);
            var subscriptionDto = new SubscriptionDto
            {
                Id = 0,
                PublisherServiceClass = "TEST",
                PublisherToken = "TEST",
                PublisherUserName = "TEST",
                SubscriberId = "TEST",
                SubscriberUserName = "TEST"
            };
            repositoryMock.Setup(m => m.Get(It.IsAny<int>())).Returns((Subscription) null);

            subscriptionService.Unsubscribe(subscriptionDto);

            repositoryMock.Verify(m => m.Delete(It.Is<int>(i => i == subscriptionDto.Id)), Times.Never);
            unitOfWorkMock.Verify(m => m.Save(), Times.Never);
        }

        [Test]
        public void GetByUserName()
        {
            var subscriptionService = new SubscriptionService(unitOfWorkMock.Object);
            var subscription = new Subscription
            {
                Id = 0,
                PublisherServiceClass = "TEST",
                PublisherToken = "TEST",
                PublisherUserName = "TEST",
                SubscriberId = "TEST",
                SubscriberUserName = "TEST"
            };
            repositoryMock.Setup(m => m.Find(It.IsAny<Func<Subscription, bool>>()))
                .Returns(new List<Subscription> { subscription });

            var subscriptionDto = subscriptionService.GetByUserName("TEST");

            Assert.AreEqual(subscription.Id, subscriptionDto.Id);
            Assert.AreEqual(subscription.PublisherServiceClass, subscriptionDto.PublisherServiceClass);
            Assert.AreEqual(subscription.PublisherToken, subscriptionDto.PublisherToken);
            Assert.AreEqual(subscription.PublisherUserName, subscriptionDto.PublisherUserName);
            Assert.AreEqual(subscription.SubscriberId, subscriptionDto.SubscriberId);
            Assert.AreEqual(subscription.SubscriberUserName, subscriptionDto.SubscriberUserName);
        }

        [Test]
        public void GetByUserName_When_Subscription_Not_Exists()
        {
            var subscriptionService = new SubscriptionService(unitOfWorkMock.Object);
            repositoryMock.Setup(m => m.Find(It.IsAny<Func<Subscription, bool>>()))
                .Returns(new List<Subscription>());

            var subscriptionDto = subscriptionService.GetByUserName("TEST");

            Assert.IsNull(subscriptionDto);
        }

        [Test]
        public void GetByUserId()
        {
            var subscriptionService = new SubscriptionService(unitOfWorkMock.Object);
            var subscription = new Subscription
            {
                Id = 0,
                PublisherServiceClass = "TEST",
                PublisherToken = "TEST",
                PublisherUserName = "TEST",
                SubscriberId = "TEST",
                SubscriberUserName = "TEST"
            };
            var subscriptions = new List<Subscription> {subscription, subscription, subscription};
            repositoryMock.Setup(m => m.GetAll()).Returns(subscriptions);

            var subscriptionsDto = subscriptionService.GetByUserId("TEST");

            foreach (var subscriptionDto in subscriptionsDto)
            {
                Assert.AreEqual(subscription.Id, subscriptionDto.Id);
                Assert.AreEqual(subscription.PublisherServiceClass, subscriptionDto.PublisherServiceClass);
                Assert.AreEqual(subscription.PublisherToken, subscriptionDto.PublisherToken);
                Assert.AreEqual(subscription.PublisherUserName, subscriptionDto.PublisherUserName);
                Assert.AreEqual(subscription.SubscriberId, subscriptionDto.SubscriberId);
                Assert.AreEqual(subscription.SubscriberUserName, subscriptionDto.SubscriberUserName);
            }
        }

        [Test]
        public void GetAll()
        {
            var subscriptionService = new SubscriptionService(unitOfWorkMock.Object);
            var subscription = new Subscription
            {
                Id = 0,
                PublisherServiceClass = "TEST",
                PublisherToken = "TEST",
                PublisherUserName = "TEST",
                SubscriberId = "TEST",
                SubscriberUserName = "TEST"
            };
            var subscriptions = new List<Subscription> { subscription, subscription, subscription };
            repositoryMock.Setup(m => m.GetAll()).Returns(subscriptions);

            var subscriptionsDto = subscriptionService.GetAll();

            foreach (var subscriptionDto in subscriptionsDto)
            {
                Assert.AreEqual(subscription.Id, subscriptionDto.Id);
                Assert.AreEqual(subscription.PublisherServiceClass, subscriptionDto.PublisherServiceClass);
                Assert.AreEqual(subscription.PublisherToken, subscriptionDto.PublisherToken);
                Assert.AreEqual(subscription.PublisherUserName, subscriptionDto.PublisherUserName);
                Assert.AreEqual(subscription.SubscriberId, subscriptionDto.SubscriberId);
                Assert.AreEqual(subscription.SubscriberUserName, subscriptionDto.SubscriberUserName);
            }
        }

        [Test]
        public void UpdateServiceClass()
        {
            var subscriptionService = new SubscriptionService(unitOfWorkMock.Object);
            var subscriptionDto = new SubscriptionDto
            {
                Id = 0,
                PublisherServiceClass = "UPDATED",
                PublisherToken = "TEST",
                PublisherUserName = "TEST",
                SubscriberId = "TEST",
                SubscriberUserName = "TEST"
            };
            var subscription = new Subscription
            {
                Id = 0,
                PublisherServiceClass = "TEST",
                PublisherToken = "TEST",
                PublisherUserName = "TEST",
                SubscriberId = "TEST",
                SubscriberUserName = "TEST"
            };
            repositoryMock.Setup(m => m.Find(It.IsAny<Func<Subscription, bool>>()))
                .Returns(new List<Subscription> {subscription});

            subscriptionService.UpdateServiceClass(subscriptionDto);

            Assert.AreEqual(subscription.PublisherServiceClass, subscriptionDto.PublisherServiceClass);
            repositoryMock.Verify(n => n.Update(It.Is<Subscription>(m =>
                m.Id == subscriptionDto.Id && m.PublisherUserName == subscriptionDto.PublisherUserName &&
                m.PublisherServiceClass == subscriptionDto.PublisherServiceClass &&
                m.PublisherToken == subscriptionDto.PublisherToken &&
                m.SubscriberId == subscriptionDto.SubscriberId &&
                m.SubscriberUserName == subscriptionDto.SubscriberUserName)), Times.Once);
            unitOfWorkMock.Verify(m => m.Save(), Times.Once);
        }

        [Test]
        public void UpdateServiceClass_When_Subscription_Not_Exists()
        {
            var subscriptionService = new SubscriptionService(unitOfWorkMock.Object);
            repositoryMock.Setup(m => m.Find(It.IsAny<Func<Subscription, bool>>()))
                .Returns(new List<Subscription>());

            subscriptionService.UpdateServiceClass(new SubscriptionDto());

            repositoryMock.Verify(n => n.Update(It.IsAny<Subscription>()), Times.Never);
            unitOfWorkMock.Verify(m => m.Save(), Times.Never);
        }

        [Test]
        public void Dispose()
        {
            var subscriptionService = new SubscriptionService(unitOfWorkMock.Object);

            subscriptionService.Dispose();

            unitOfWorkMock.Verify(m => m.Dispose(), Times.Once);
        }
    }
}
