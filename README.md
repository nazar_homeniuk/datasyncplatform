# Data synchronization platform based on WebHooks

The platform provides API that allows third party applications (clients) receive notifications when some managed data is changed (user profile, accounts, statuses, etc). The API based on WebHooks. On backend we use RabbitMQ + NSB. Authentication mechanism based on tokens.
Platform overview [Wiki](https://bitbucket.org/nazar_homeniuk/datasyncplatform/wiki/Platform%20overview).

## Architecture
### Sequence diagram

Following diagram displays communication between Client site and API.

![Sequence diagram](https://bitbucket.org/nazar_homeniuk/datasyncplatform/downloads/Sequence%20diagram.svg)

### Component diagram

Following structure represents architecture of platform.

![Component diagram](https://bitbucket.org/nazar_homeniuk/datasyncplatform/downloads/Components%20diagram.svg)
 
## Built With

* [ASP.NET MVC](https://www.asp.net/mvc) - The web framework used
* [ASP.NET WebAPI](https://www.asp.net/web-api) - The web API framework used
* [ASP.NET Identity](https://www.asp.net/identity) - Authorization and authentication system used
* [ASP.NET WebHooks](https://docs.microsoft.com/en-us/aspnet/webhooks/) - WebHooks library used
* [ADO.NET Entity Framework](https://docs.microsoft.com/ef/) - ORM framework used
* [NServiceBus](https://particular.net/nservicebus) - Service bus for .NET
* [RabbitMQ](https://www.rabbitmq.com/) - Message broker used
* [Ninject](http://www.ninject.org/) - Dependency injector used
* [AutoMapper](https://automapper.org/) - A convention-based object-object mapper used


## Authors

* **Nazar Homeniuk** - *Initial work*